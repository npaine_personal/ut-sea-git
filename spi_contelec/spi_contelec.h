#ifndef _SPI_CONTELEC_H
#define _SPI_CONTELEC_H

#ifndef SPI_BASE_ADDR
#define SPI_BASE_ADDR 0x1D8
#endif

#define SPI_Control(addr) ( (addr) + 0x0 )
#define SPI_Status(addr)  ( (addr) + 0x1 )
#define SPI_Data0(addr)   ( (addr) + 0x2 )
#define SPI_Data1(addr)   ( (addr) + 0x3 )
#define SPI_Data2(addr)   ( (addr) + 0x4 )
#define SPI_Data3(addr)   ( (addr) + 0x5 )

#ifndef SPI_ADDR_SZ
#define SPI_ADDR_SZ 0x6 //number of IO addresses needed 
#endif

//data lengths
enum{Size_8bit, Size_16bit, Size_24bit, Size_32bit};

//slave select
enum{AUTOMATIC_SS, MANUAL_SS};

//spi clock frequency
enum{Freq_1042KHz, Freq_2083KHz, Freq_4167KHz, Freq_8333KHz};

//SPI IRQ enable
enum{SPI_IRQ_DISABLED, SPI_IRQ_ENABLED};

//SPI IRQ enable
enum{MSBIT_1ST, LSBIT_1ST};

//Contelec shift out value (push this into sensor to get data out)
#define CONTELEC_OUT_MSB 0xAA
#define CONTELEC_OUT_LSB 0xFF
#define CONTELEC_HI_Z 0xFF //sending FF's puts DIO pin in HI-Z state (necessary when reading sensor data)

//API error return values
enum{ERR_NONE, ERR};

//inputs: slave select number
//output: contelec sensor value
int getContelecSensorValue(uint8_t ss, int *value);

//initializes SPI registers for reading Contelec sensors
void SPI_Init(void);

#endif //_ANALOG_IN_H
