//spi input test code
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <utils.h>
#include "spi_contelec.h"

int main(void) {
  int value, i;

  IOPermission(SPI_BASE_ADDR, SPI_ADDR_SZ);
  SPI_Init();

  while(1){
    for(i=0; i<4; i++){
      getContelecSensorValue(i, &value);
      printf("SPI SS%d value is: %d\n", i, value);
    }

    printf("\n");
    usleep(1000000);
  }

  return 0;
}
