//spi contelec driver
//reads from contelec (novotechnik) senosrs
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <utils.h>
#include "spi_contelec.h"

static unsigned int base = SPI_BASE_ADDR;

volatile union {
   uint8_t value;
   struct {
     unsigned SS :3;
     unsigned MAN_SS :1;
     unsigned SPILEN :2;
     unsigned CPHA :1;
     unsigned CPOL :1;
   } bits;
} SPI_Control;

volatile union {
   uint8_t value;
   struct {
     unsigned BUSY :1;
     unsigned HW_INT :1;
     unsigned LSBIT_1ST :1;
     unsigned HW_IRQ_EN :1;
     unsigned SPICLK :2;
     unsigned IRQSEL :2;
   } bits;
} SPI_Status;

//inputs: slave select number (0-3)
//output: contelec sensor value
int getContelecSensorValue(uint8_t ss, int *value){
  uint8_t sensor_lsb, sensor_msb;
  SPI_Control.bits.SS = ss + 1; //select which slave to sample from
  outbv(SPI_Control.value,SPI_Control(base));//select spi slave

  outbv(CONTELEC_OUT_MSB,SPI_Data3(base)); //LSB of 16-bit transaction goes into SPI_Data2 (versalogic manual)
  outbv(CONTELEC_OUT_LSB,SPI_Data3(base)); //MSB of 16-bit transaction goes into SPI_Data3 and initiates SPI transaction (versalogic manual)

  //no need to usleep, its too inaccurate for this, two separate transactions inserts enough time between transactions
  //usleep(20); //wait and then read data back from sensor (TODO: change to realtime)

  //read sensor data
  outbv(CONTELEC_HI_Z,SPI_Data3(base)); //dummy write (actually is shifting in read data)
  sensor_msb = inbv(SPI_Data3(base)); //read MSB
  outbv(CONTELEC_HI_Z,SPI_Data3(base)); //dummy write (actually is shifting in read data)
  sensor_lsb = inbv(SPI_Data3(base)); //read LSB

  *value = (int)((sensor_msb << 8) | sensor_lsb); //combine into 16-bit value
#if SHOW_TRANSACTIONS
  printf("SPI: Recieved value from sensor: 0x%X\n", *value);
#endif

  return ERR_NONE;
}

//initializes SPI registers for reading Contelec sensors
void SPI_Init(void){
#if SHOW_TRANSACTIONS
  printf("SPI: Using base address: 0x%X\n", base);
#endif

  SPI_Status.value = 0;
  SPI_Control.value = 0;

  SPI_Control.bits.CPOL = 0;
  SPI_Control.bits.CPHA = 1;
  SPI_Control.bits.SPILEN = Size_8bit;
  SPI_Control.bits.MAN_SS = MANUAL_SS;

  SPI_Status.bits.SPICLK = Freq_1042KHz;
  SPI_Status.bits.HW_IRQ_EN = SPI_IRQ_DISABLED;
  SPI_Status.bits.LSBIT_1ST = MSBIT_1ST;

#if SHOW_TRANSACTIONS
  printf("SPI: Setting SPI_Control to: 0x%X\n", SPI_Control.value);
  printf("SPI: Setting SPI_Status to: 0x%X\n", SPI_Status.value);
#endif

  outbv(SPI_Control.value,SPI_Control(base));//configure spi control register
  outbv(SPI_Status.value,SPI_Status(base));//configure spi status register

#if SHOW_TRANSACTIONS
  printf("SPI: Reading contents of SPI_Control: 0x%X\n", inbv(SPI_Control(base)));
  printf("SPI: Reading contents of SPI_Status: 0x%X\n", inbv(SPI_Status(base)));
  printf("SPI: Configuration complete.\n");
#endif
}


