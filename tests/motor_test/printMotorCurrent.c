//reads monitored current from elmo and prints to screen
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <analog_in.h>
#include <math.h>
#include <utils.h>

//analog input channels
#define A_MOTOR1_CURRENT_CHAN 1
#define A_MOTOR2_CURRENT_CHAN 0

#define USEC_PER_SEC 1000000

#define MOTOR1_CURRENT_OFFSET_ADJUST_V -0.057 //volts at DAC output
#define MOTOR2_CURRENT_OFFSET_ADJUST_V -0.062 //volts at DAC output
#define MOTOR_TORQUE_DIRECTION -1
#define EN_MIRROR_OUTPUT 1
#define DAC_VOLTAGE 10.0
#define ELMO_PEAK_CURRENT 30.0
#define currentOut2dacVoltage(current) ((float)(current)*DAC_VOLTAGE/ELMO_PEAK_CURRENT)

#define AMPLITUDE_DEFAULT 0.0

static int End=0;

static void endme(int dummy) { End=1; }

int main(int argc, char **argv) {
   float adc_voltage;

   signal(SIGINT, endme); //ctrl-c out gracefully

   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogIn_Init();
  
   
   while(!End){
     if( sampleADC_5VB(A_MOTOR1_CURRENT_CHAN, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", A_MOTOR1_CURRENT_CHAN, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",A_MOTOR1_CURRENT_CHAN); }
     if( sampleADC_5VB(A_MOTOR2_CURRENT_CHAN, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", A_MOTOR2_CURRENT_CHAN, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",A_MOTOR2_CURRENT_CHAN); }
     printf("\n");
     usleep(200000);
   }

   printf("Ending..\n");

   return 0;
}



