//timing code for regular linux timing functions
//toggles all ports high and low
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>
#include <utils.h>
#include <digital_out.h>

#define CYCLE_RATE_HZ 1000
#define USEC_PER_SEC 1000000

static int End=0;

static void endme(int dummy) { End=1; }

int main(void) {

   signal(SIGINT, endme); //ctrl-c out gracefully
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   DigitalOut_Init();

   printf("Toggling all digital outputs at %d hz\n", CYCLE_RATE_HZ);

   while(!End){
     togglePortA(0xFF);
     togglePortB(0xFF);
     togglePortC(0xFF);
     usleep(USEC_PER_SEC/CYCLE_RATE_HZ);
   }
   printf(" ending..\n");
   return 0;
}

