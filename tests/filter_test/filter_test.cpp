//filter test code
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <math.h>
#include <utils.h>
#include <classyFilters.hpp>
#include "../../actuator/actuator_params.h"
#include "../../actuator/actuator.hpp"

#define LOOP_FREQ_HZ 100.0

#define LOGFILE_DEFAULT "filter_data.csv" //tab-separated values
#define FILE_DELIMITER ","
#define CONSOLE_DELIMITER "\t"

static char *LogFile;
static char File_default[] = LOGFILE_DEFAULT;

static int End=0;
static void endme(int dummy) { End=1; }

int main(int argc, char **argv) {
   struct timeval tvStart, tvCurrent;
   long int elapsedTime_us, currentTime_us, sampleTime_prev_us=0;
   double samplePeriod_s;

   printf("arm_inertia %f\n", ARM_INERTIA);

   printf("tdobFilt: fc=%d Hz, m=%d, b=%d, k=%d, kp=%0.2f Hz, kd=%0.3f Hz, kd_fc=%d Hz, N=%0.1f Hz, eff=%0.1f Hz, k_tau=%0.3f\n", 
      FCONTROL1_Q_FC, M_FIT, B_FIT, SPRING_CONST_NpM, FCONTROL1_KP, FCONTROL1_KD, FCONTROL1_KD_FC, SPEED_REDUCTION, DRIVETRAIN_EFF, TORQUE_CONSTANT);

   float v_fc_hz = 10;
   float lpf_fc_hz = 10;

   float lpFiltOut, vFiltOut;

   //file stuff
   LogFile = File_default;
   FILE* ifp;
   ifp = fopen(LogFile, "w");
   if (!ifp) {
     fprintf(stderr, "Cannot open file %s\n", LogFile);
     exit(1);
   }

   VelocityFilter vfilt(2, 1/LOOP_FREQ_HZ);
   LowPassFilter lpfilt(2, 1/LOOP_FREQ_HZ);

   vfilt.calculateCoeffs(v_fc_hz);
   vfilt.reset();

   lpfilt.calculateCoeffs(lpf_fc_hz);
   lpfilt.reset();

   signal(SIGINT, endme); //ctrl-c out gracefully

   //print header
   printf("Data logging to %s at %f hz\n", LogFile, LOOP_FREQ_HZ);

   fprintf(ifp, "Time (sec)" FILE_DELIMITER 
          "Sample Period (sec)" FILE_DELIMITER 
          "Vel Filt" FILE_DELIMITER 
          "LP Filt\n");

   //get starting time
   gettimeofday(&tvStart, NULL);
   
   while(!End){
     gettimeofday(&tvCurrent, NULL);
     currentTime_us = tvCurrent.tv_sec*USEC_PER_SEC + tvCurrent.tv_usec;
     elapsedTime_us = currentTime_us - (tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec);
     samplePeriod_s = us2sec(currentTime_us - sampleTime_prev_us);
     sampleTime_prev_us = currentTime_us;

     vFiltOut = vfilt.next(1);
     lpFiltOut = lpfilt.next(1);

     //printf("out: %f\n", lpFiltOut);

     fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", 
          us2sec(elapsedTime_us), 
          samplePeriod_s, 
          vFiltOut,
          lpFiltOut);


     usleep(USEC_PER_SEC/LOOP_FREQ_HZ);
   }

   printf("Ending..\n");

   return 0;
}



