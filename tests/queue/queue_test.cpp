#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <stdlib.h> 

using namespace std;

int main ()
{
  queue<float> data;

  /*
  cout << "pushing " << 0.0 << endl;
  data.push(0.0);
  cout << "pushing " << 0.1 << endl;
  data.push(0.1);
 
  cout << "data has " << data.size() << " elements\n";

  cout << "popping " << data.front() << endl;
  data.pop();
  cout << "data has " << data.size() << " elements\n";
  cout << "popping " << data.front() << endl;
  data.pop();
  cout << "data has " << data.size() << " elements\n";
  */

  int i;

  for(i=0; i<5; i++){
    data.push(0.0);
  }

  cout << "data has " << data.size() << " elements\n";

  return 0;
}
