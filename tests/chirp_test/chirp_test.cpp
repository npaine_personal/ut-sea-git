//test of the up-down linear chirp code used by test_presponse and contained within referenceSignals.c
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <math.h>
#include <utils.h> 
#include <referenceSignals.h> 
 
#define LOOP_FREQ_HZ 10.0
 
static unsigned char End=0;
static void endme(int dummy) { End=1; }

int main(int argc, char **argv) {
   struct timeval tvStart, tvCurrent;
   long int elapsedTime_us, currentTime_us;
   float elapsedTime_s;
   //long int sampleTime_prev_us=0;
   //double samplePeriod_s;

   float desiredPosition_rad, desiredVelocity_radps;
   float switching_freq_hz = 1.5;
   float sweep_rate = 0.1;
   float offset = 100.0;
   float amplitude = 10.0;

   printf("Chirp test\n");
   printf("Chirp params:\n");
   printf(" Max freq: %f\n",switching_freq_hz);
   printf(" Sweep rate: %f\n",sweep_rate);
   printf(" Offset: %f\n",offset);
   printf(" Amplitude: %f\n",amplitude);

   printf("Starting...\n");

   signal(SIGINT, endme); //ctrl-c out gracefully

   //get starting time
   gettimeofday(&tvStart, NULL);
   
   while(!End){
     gettimeofday(&tvCurrent, NULL);
     currentTime_us = tvCurrent.tv_sec*USEC_PER_SEC + tvCurrent.tv_usec;
     elapsedTime_us = currentTime_us - (tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec);
     elapsedTime_s = us2sec(elapsedTime_us);
     //samplePeriod_s = us2sec(currentTime_us - sampleTime_prev_us);
     //sampleTime_prev_us = currentTime_us;


     getDesiredPosition(&desiredPosition_rad, &desiredVelocity_radps, elapsedTime_s, 53.0, offset, amplitude, switching_freq_hz, sweep_rate, CHIRP, true, &End);
 
     fflush(stdout);

     usleep(USEC_PER_SEC/LOOP_FREQ_HZ);
   }

   printf("Ending..\n");

   return 0;
}



