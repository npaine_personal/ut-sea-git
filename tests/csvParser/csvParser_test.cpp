#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h> 
#include <csvParser.hpp>

using namespace std;

int main ()
{
  string filename("KneeJointData_Slow.csv");

  csv_entry timeStamp, posDes, velDes;
  timeStamp.idx = 1;
  posDes.idx = 2;
  velDes.idx = 3;

  readCSVFile(filename, 3, &timeStamp, &posDes, &velDes);
  
  // cout << "Time vector: ";
  // for( vector<float>::const_iterator i = timeStamp.data.begin(); i != timeStamp.data.end(); ++i)
  //   cout << *i << ' ';
  // cout << endl;

  // cout << "posDes vector: ";
  // for( vector<float>::const_iterator i = posDes.data.begin(); i != posDes.data.end(); ++i)
  //   cout << *i << ' ';
  // cout << endl;

  // cout << "velDes vector: ";
  // for( vector<float>::const_iterator i = velDes.data.begin(); i != velDes.data.end(); ++i)
  //   cout << *i << ' ';
  // cout << endl;
  

  cout << "time vector has size " << timeStamp.data.size() << endl;
  cout << "max vector size is " << timeStamp.data.max_size() << endl;
  //cout << "time[1] is " << timeStamp.data[1] << endl;

  return 0;
}
