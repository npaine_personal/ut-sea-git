//analog output test code
//make sure the board jumpers correspond to the type of DAC output funtions used
// ex. 5VU, 10Vu, 5VB, 10VB
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>
#include <utils.h>
#include "analog_out.h"

static int End=0;

static void endme(int dummy) { End=1; }

int main(void) {
   int out=0, inc=1;

   signal(SIGINT, endme); //ctrl-c out gracefully
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   AnalogOut_Enable(); //enables analog outputs

   printf("Generating sawtooth waveform on all DAC outputs\n");

   while(!End){
//     //outputDAC_raw(1, 1024);

     outputDAC_raw(1, out);
     outputDAC_raw(2, out);
     outputDAC_raw(3, out);
     outputDAC_raw(4, out);

     if(out >= DAC_PRECISION-1){inc = -1;}
     else if(out <= 0){inc = 1;}

     out += inc;

     usleep(2000);

   }

   AnalogOut_Disable();

   return 0;
}

