#ifndef _ANALOG_OUT_H
#define _ANALOG_OUT_H

#define SHOW_TRANSACTIONS 0

#ifndef AIO_BASE_ADDR
#define AIO_BASE_ADDR 0x300
#endif

#define DAC_PRECISION 4096

//zeroing (due to the jumpers on the board, zero may be either 0 or DAC_PRECISION/2 (unipolar vs bipolar)
#define DAC1_ZERO (DAC_PRECISION/2)
#define DAC2_ZERO (DAC_PRECISION/2)
#define DAC3_ZERO (DAC_PRECISION/2)
#define DAC4_ZERO (DAC_PRECISION/2)

#define DAC_CH1(addr)     ( (addr) + 0x4 )
#define DAC_CH1_MSB(addr) ( (addr) + 0x5 )
#define DAC_CH2(addr)     ( (addr) + 0x6 )
#define DAC_CH2_MSB(addr) ( (addr) + 0x7 )
#define DAC_CH3(addr)     ( (addr) + 0x8 )
#define DAC_CH3_MSB(addr) ( (addr) + 0x9 )
#define DAC_CH4(addr)     ( (addr) + 0xA )
#define DAC_CH4_MSB(addr) ( (addr) + 0xB )
#define DAC_REF_EN(addr)  ( (addr) + 0x18 )

#ifndef AIO_ADDR_SZ
#define AIO_ADDR_SZ 0x19 //number of IO addresses needed 
#endif

#define DAC_5VU_GAIN 1.22
#define DAC_5VU_OFFSET 0
#define DAC_10VU_GAIN 2.44
#define DAC_10VU_OFFSET 0
#define DAC_5VB_GAIN 2.44
#define DAC_5VB_OFFSET -5
#define DAC_10VB_GAIN 4.88
#define DAC_10VB_OFFSET -10

#define DAC_VREF 4.096

//inputs: DAC channel (1-4), value to output (raw, 12-bit)
//output: none
void outputDAC_raw(uint8_t chan, uint16_t out_val);

void outputDAC_5VU(uint8_t chan, float voltage);

void outputDAC_10VU(uint8_t chan, float voltage);

void outputDAC_5VB(uint8_t chan, float voltage);

void outputDAC_10VB(uint8_t chan, float voltage);

//call this first
void AnalogOut_Init(void);

//set all DAC outputs to zero
void AnalogOut_Zero(void);

//enable DAC outputs
void AnalogOut_Enable(void);

//disables analog outputs, but saves output value
//AnalogOut_Enable restores previous output value
void AnalogOut_Disable(void);

#endif //_ANALOG_OUT_H
