# README #

### What is this repository for? ###

* Control of the UT-SEA actuators in the HCRL laboratory at UT Austin.

### How do I get set up? ###

* Make sure RTAI is installed
* Build code by running make
* Run ./load_modules.sh to load RTAI kernel modules (required for low-jitter timing)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* n.a.paine@gmail.com

### Directory Structure ###

actuator - Main directory containing all force, position, and impedance control code
utils - Directory used by many pieces of code, contains basic utility functions
tests - Unit tests of various sorts

Drivers:
	analog_out
	analog_in
	digital_out
	quadrature
	spi_contelec (unused)
