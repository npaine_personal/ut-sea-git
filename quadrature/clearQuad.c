//clear quadruature counders
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <utils.h>
#include "quadrature.h"

int main(void) {

   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   Quad_init(512, 20000, 500, 500, 500, 512, 16384, 500);
   Quad_resetCounters();

   return 0;
}

