#ifndef _REF_SIG_H
#define _REF_SIG_H

#include <vector>

//force params
#define F_SETPOINT_SWITCH_FREQ_HZ_DEFAULT 0.5
#define F_SETPOINT_FORCE_AMPLITUDE_N_DEFAULT 100.0
#define F_SETPOINT_MID_FORCE_N 0.0
#define F_SETPOINT_BLEND_SLEWRATE 2.0 //seconds

//force chirp params
#define F_SWEEP_FREQ_HZ_LOW 0.001
#define F_SWEEP_FREQ_HZ_HIGH 200
#define F_SWEEP_RATE 0.010 //percent change of sweep range per second
#define F_SWEEP_RANGE (F_SWEEP_FREQ_HZ_HIGH-F_SWEEP_FREQ_HZ_LOW)
#define F_SWEEP_SETPOINT_FORCE_AMPLITUDE_N_DEFAULT 20.0
//#define F_SWEEP_SETPOINT_MID_FORCE_N 60.0 //now using grav comp

//position params
#define P_SETPOINT_SWITCH_FREQ_HZ_DEFAULT 0.5
#define P_SETPOINT_POS_AMPLITUDE_DEG_DEFAULT 10.0
#define P_SETPOINT_OFFSET_POS_DEG 100.0 //MID_ARM_ANGLE_DEG //90.0 //100.0
#define P_SETPOINT_BLEND_SLEWRATE 1.0 //seconds
#define P_SETPOINT_POS_AMPLITUDE_M_DEFAULT 0.01

//position chirp params
#define P_SWEEP_FREQ_HZ_LOW 0.001
#define P_SWEEP_FREQ_HZ_HIGH 1.5
#define P_SWEEP_RATE 0.1 //percent change of sweep range per second

//position spline params
#define SPLINE_SWITCH_RATIO 0.3
#define SPLINE_HOLD 1.0 //wait time between splines

//control modes
enum{ CHIRP, CONST, SQUARE, SINE, SPLINE };

//chirp types
enum{ LINEAR, EXP };

float getChirpSignal(float elapsedTime, float samplePeriod, float amplitude, float offset, float lowFreq_hz, float highFreq_hz, float rate, unsigned char *end);

//send a variety of reference signals as desired force
//returns desired force
float getDesiredActuatorForce(float elapsedTime, float samplePeriod, float currentForce, float offset, float amplitude, float switching_freq_hz, unsigned char mode, unsigned char blend, unsigned char *end);

//generates a variety of reference signals as desired position or angle
//returns desired position or angle
void getDesiredPosition(float *desiredPosition, float *desiredVelocity, float elapsedTime, float currentPosition, float offset, float amplitude, float switching_freq_hz, float sweep_rate, unsigned char mode, unsigned char blend, unsigned char *end);

void getDesiredPositionFromArray(float *desiredPosition, float *desiredVelocity, float elapsedTime, float currentPosition, std::vector<float> time, std::vector<float> posDes, std::vector<float> velDes, unsigned char blend, unsigned char *end);
 
#endif //_REF_SIG_H
