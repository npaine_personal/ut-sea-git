#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include "utils.h"
#include <math.h>
#include "referenceSignals.h"

static float InitialPosition=0.0, InitialVelocity=0.0, InitialForce=0.0;
static unsigned char FirstRun_P=1, FirstRun_F=1;
static unsigned char Direction = 1;
static float ChirpSwitchTime = 0.0;
static float Effective_switching_freq_hz=0.0;
static float Prev_effective_angle=0.0;
static float Prev_desiredPosition=0.0;
static unsigned char zeroCrossing=0;
static unsigned char Spline_sequencer=0;
static float Spline_start_time=0.0, Spline_duration=0.0, Spline_finish_time=0.0;
static float Spline_A, Spline_B, Spline_C, Spline_F, Spline_final;
static unsigned int Sig_array_idx=0;

//generates a chirp signal
float getChirpSignal(float elapsedTime, float samplePeriod, float amplitude, float offset, float lowFreq_hz, float highFreq_hz, float rate, unsigned char *end){
  float effective_angle, effective_switching_freq_hz, chirp_signal;
  float range = highFreq_hz - lowFreq_hz;

  if(elapsedTime > 0.0){

    //exponential chirp
    effective_angle = 2 * PI * lowFreq_hz * ( pow(rate*range,elapsedTime) - 1 )/log(rate*range);
    chirp_signal = amplitude*sin(effective_angle) + offset;
         
    if(samplePeriod <= 0.0){
      effective_switching_freq_hz = 0.0;
    }
    else{
      effective_switching_freq_hz = (effective_angle - Prev_effective_angle)/samplePeriod/2/PI; 
    }
    Prev_effective_angle = effective_angle;
    if(effective_switching_freq_hz > highFreq_hz) {
      *end = 1;
    }
  }
  else{
    chirp_signal = offset;
    effective_switching_freq_hz = 0.0;
  }

  return chirp_signal;
}

//returns desired force
//max freq for chirp signal set statically
float getDesiredActuatorForce(float elapsedTime, float samplePeriod, float currentForce, float offset, float amplitude, float switching_freq_hz, unsigned char mode, unsigned char blend, unsigned char *end){

  float force_des_n;

  if(FirstRun_F){
    InitialForce = currentForce; 
  }

  switch(mode){
    case CHIRP:
      force_des_n = getChirpSignal(elapsedTime, samplePeriod, amplitude, offset, F_SWEEP_FREQ_HZ_LOW, switching_freq_hz, F_SWEEP_RATE, end);
      break;
    case CONST:
      force_des_n = amplitude;
      //printf("tg:%f\tang:%f\n", armGravityTorque(arm_angle_rad), rad2deg(arm_angle_rad) );
      break;
    case SQUARE:
      if((int)(elapsedTime*2*switching_freq_hz)%2) { 
        force_des_n = offset + amplitude; 
      }
      else {  
        force_des_n = offset - amplitude; 
      }
      break;
    case SINE:
      force_des_n = amplitude*sin(elapsedTime * 2 * PI * switching_freq_hz) + offset;
      break;
    default:    
      force_des_n = 0.0;
      break;
  }

  if(elapsedTime < F_SETPOINT_BLEND_SLEWRATE && blend){
    force_des_n = elapsedTime/F_SETPOINT_BLEND_SLEWRATE * force_des_n + (1 - elapsedTime/F_SETPOINT_BLEND_SLEWRATE) * InitialForce;
  }

  //printf("force_des_n:%f, time:%f\n",force_des_n, elapsedTime);
  FirstRun_F = 0;
  return force_des_n;
}

//calculates coefficients for the spline of the form 
// angle(t) = a*t^5 + b*t^4 + c*t^3 + angle_init where angle(time_final) = angle_final
static void calcSplineCoeffs(float angle_init, float angle_final, float time_final,  float *a,  float *b,  float *c,  float *f){
  *a = 6*(angle_final - angle_init) / pow(time_final,5);
  *b = 15*(angle_init - angle_final) / pow(time_final,4);
  *c = 10*(angle_final - angle_init) / pow(time_final,3);
  *f = angle_init;
}

//return spline value
// angle(t) = a*t^5 + b*t^4 + c*t^3 + angle_init where angle(time_final) = angle_final
static float getSplineValue(float time, float a, float b, float c, float f){
  //printf("time: %f\n", time);
  return a*pow(time,5) + b*pow(time,4) + c*pow(time,3) + f;
}

//coordinate single spline
//return 1 when spline is complete, 0 otherwise
static int generateSpline(float elapsedTime, float arm_ang_init, float arm_ang_final, float spline_duration, float newSpline, float *arm_ang_des){
  float done=0;

  if(newSpline){
    calcSplineCoeffs(arm_ang_init, arm_ang_final, spline_duration, &Spline_A, &Spline_B, &Spline_C, &Spline_F);
    Spline_start_time = elapsedTime;
    Spline_duration = spline_duration;
    Spline_final = arm_ang_final;
  }

  if(elapsedTime >= Spline_start_time && elapsedTime < Spline_start_time + Spline_duration) { //check to see if we're within the spline time period
    *arm_ang_des = getSplineValue(elapsedTime - Spline_start_time, Spline_A, Spline_B, Spline_C, Spline_F);
  }
  else { //if not, we are already done
    *arm_ang_des = Spline_final;
    done = 1;
  }
  
  return done;
}

//returns desired angle
//max chirp freq set by switching_freq_hz
void getDesiredPosition(float *desiredPosition, float *desiredVelocity, float elapsedTime, float currentPosition, float offset, float amplitude, float switching_freq_hz, float sweep_rate, unsigned char mode, unsigned char blend, unsigned char *end){
  float t, sweep_range;
  //amplitude = deg2rad(switchingAmplitude);
  //offset = deg2rad(P_SETPOINT_MID_POS_DEG);
  sweep_range = switching_freq_hz-P_SWEEP_FREQ_HZ_LOW;
  *desiredVelocity = 0;

  //initialization
  if(FirstRun_P){
    InitialPosition = currentPosition;
    InitialVelocity = 0.0;
    Effective_switching_freq_hz=0.0;
  }

  switch(mode){
    case CONST:
      *desiredPosition = offset + amplitude;
      *desiredVelocity = 0;
      break;
    case CHIRP: //special up and down linear chirp signal
      if(elapsedTime > 0.0){   
        if(Direction == 1) {
          t = elapsedTime;
          //linear chirp
          Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW + sweep_rate*sweep_range*t;
          *desiredPosition = amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;
          *desiredVelocity = 2*PI*amplitude*cos(t * 2 * PI * Effective_switching_freq_hz )
                  *(P_SWEEP_FREQ_HZ_LOW + 2*sweep_rate*sweep_range*t);
 
          if((*desiredPosition-offset) * (Prev_desiredPosition-offset) < 0) { zeroCrossing = 1;}
          else{ zeroCrossing = 0; }
          Prev_desiredPosition = *desiredPosition;

          //exponential chirp
          //Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW * (pow(sweep_rate*sweep_range,t)-1)/log(sweep_rate*sweep_range)/t;
          //force_des_n = amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;
          if(Effective_switching_freq_hz >= switching_freq_hz && zeroCrossing ) {
            Direction = -1;
            ChirpSwitchTime = elapsedTime;
            //printf("switching\n");
          }
        }
        else{
          t = ChirpSwitchTime - (elapsedTime-ChirpSwitchTime);

          if(t > elapsedTime || Effective_switching_freq_hz <= 0.0){
            *end = 1;
            *desiredPosition = offset;
          }
          else{
            //linear chirp
            Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW + sweep_rate*sweep_range*t;
            *desiredPosition = -amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;
            *desiredVelocity = 2*PI*amplitude*cos(t * 2 * PI * Effective_switching_freq_hz )
                  *(P_SWEEP_FREQ_HZ_LOW + 2*sweep_rate*sweep_range*t);

            //exponential chirp
            //Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW * (pow(sweep_rate*sweep_range,t)-1)/log(sweep_rate*sweep_range)/t;
            //force_des_n = amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;
          }
        }
        //printf("%f\t%f\t%f\t%d\n",elapsedTime, Effective_switching_freq_hz, *desiredPosition, zeroCrossing );
      }
      else{
        *desiredPosition = offset;
      }
      break;
    case SQUARE:
      if((int)(elapsedTime*2*switching_freq_hz)%2) { 
	    *desiredPosition = offset + amplitude; 
	    }
        else {
	      *desiredPosition = offset - amplitude; 
	    }
      //printf("%d %f %f ", offset, amplitude);
      break;
    case SINE:
      *desiredPosition = amplitude*sin(elapsedTime * 2 * PI * switching_freq_hz) + offset;
      *desiredVelocity = 2*PI*switching_freq_hz*amplitude*cos(elapsedTime * 2 * PI * switching_freq_hz);
      break;
    case SPLINE: 
      //coordinate multi-spline sequence
      switch(Spline_sequencer){
        case 0: //start first spline
          //printf("spline sequence 0\n");
          (int)generateSpline(elapsedTime, offset - amplitude, offset + amplitude, SPLINE_SWITCH_RATIO/switching_freq_hz, 1, desiredPosition);
          Spline_sequencer++;
          break;
        case 1: //run until spline finishes
          //printf("spline sequence 1\n");
          if(generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, desiredPosition)){
            Spline_sequencer++;
            Spline_finish_time = elapsedTime;
          }
          break;
        case 2: //hold position for a bit before starting next spline
          //printf("spline sequence 2\n");
          if(elapsedTime < Spline_finish_time + SPLINE_HOLD){
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, desiredPosition);
          }
          else {
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, desiredPosition);
            Spline_sequencer++;
          }
          break;
        case 3: //start second spline
          //printf("spline sequence 3\n");
          (int)generateSpline(elapsedTime, offset + amplitude, offset - amplitude, (1.0-SPLINE_SWITCH_RATIO)/switching_freq_hz, 1, desiredPosition);
          Spline_sequencer++;
          break;
        case 4: //run until spline finishes
          //printf("spline sequence 4\n");
          if(generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, desiredPosition)){
            Spline_sequencer++;
            Spline_finish_time = elapsedTime;
          }
          break;
        case 5: //hold position for a bit before starting next spline
          //printf("spline sequence 5\n");
          if(elapsedTime < Spline_finish_time + SPLINE_HOLD){
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, desiredPosition);
          }
          else {
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, desiredPosition);
            Spline_sequencer = 0;
          }
          break;
        default:
          *desiredPosition = currentPosition;
          break;
        }
        break;
    default:
      *desiredPosition = currentPosition;
      break;
  }
  
  //blend to avoid steps upon startup 
  if(elapsedTime < P_SETPOINT_BLEND_SLEWRATE && blend){
    *desiredPosition = elapsedTime/P_SETPOINT_BLEND_SLEWRATE * *desiredPosition + (1 - elapsedTime/P_SETPOINT_BLEND_SLEWRATE) * InitialPosition;
    *desiredVelocity = elapsedTime/P_SETPOINT_BLEND_SLEWRATE * *desiredVelocity + (1 - elapsedTime/P_SETPOINT_BLEND_SLEWRATE) * InitialVelocity;
  }

  FirstRun_P = 0;
}

void getDesiredPositionFromArray(float *desiredPosition, float *desiredVelocity, float elapsedTime, float currentPosition, std::vector<float> time, std::vector<float> posDes, std::vector<float> velDes, unsigned char blend, unsigned char *end){

  //initialization
  if(FirstRun_P){
    InitialPosition = currentPosition;
    InitialVelocity = 0.0;
  }

  //generate trajector from vector fields
  while(time[Sig_array_idx] <= elapsedTime && Sig_array_idx <= time.size()){
    Sig_array_idx++;
  }

  if(Sig_array_idx == 0){
    *desiredPosition = posDes[Sig_array_idx]; 
    *desiredVelocity = velDes[Sig_array_idx];
  }
  else if(Sig_array_idx > time.size()){
    *end = 1;
  }
  else{
    *desiredPosition = posDes[Sig_array_idx-1]; 
    *desiredVelocity = velDes[Sig_array_idx-1];
  }

  //blend to avoid steps upon startup
  if(elapsedTime < P_SETPOINT_BLEND_SLEWRATE && blend){
    *desiredPosition = elapsedTime/P_SETPOINT_BLEND_SLEWRATE * *desiredPosition + (1 - elapsedTime/P_SETPOINT_BLEND_SLEWRATE) * InitialPosition;
    *desiredVelocity = elapsedTime/P_SETPOINT_BLEND_SLEWRATE * *desiredVelocity + (1 - elapsedTime/P_SETPOINT_BLEND_SLEWRATE) * InitialVelocity;
  }

  FirstRun_P = 0;

  return;
}


