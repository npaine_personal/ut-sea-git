#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h> 
#include <stdarg.h>
#include "csvParser.hpp"

//parses filename into vectors containing columns of the csv file
//column indexes should be referenced from 1 not 0
int readCSVFile(std::string filename, int numCols, ...)
{
  int token_idx, line_idx, j;
  std::ifstream input(filename.c_str());
  std::string line;
  csv_entry ** colList;

  colList = new csv_entry*[numCols];

  va_list vl;
  va_start(vl, numCols);

  for(j = 0; j<numCols; j++){
    colList[j] = va_arg(vl, csv_entry*);

    //printf("colList[%d] idx value is %d\n", j, colList[j]->idx);
  }

  //copy to memory
  //input.clear();
  //input.seekg (0, input.beg);
  line_idx = 0;
  while( getline( input, line ) ) {
    if(!line.empty()){
            
      if(line_idx!=0){ //skip column labels

        //std::cout << "Splitting string " << line << " into tokens (line " << line_idx << "):\n";

        std::istringstream iss(line);
        std::string token;
        token_idx = 0;
        while(getline(iss, token, ','))
        {
          token_idx++;
          //std::cout << token << std::endl;

          for(j = 0; j<numCols; j++){
            if(token_idx == colList[j]->idx){
              colList[j]->data.push_back(atof(token.c_str()));
            }
          }
        }
      }
      line_idx++;
    }
  }

  /*
  //print out
  for(j = 0; j<numCols; j++){
    std::cout << "Col #" << j << ": ";

    for( std::vector<float>::const_iterator i = colList[j]->data.begin(); i != colList[j]->data.end(); ++i)
      std::cout << *i << ' ';
    std::cout << std::endl;
  }
  */

  va_end(vl);
  delete[] colList;
  input.close();

  return 0;
}
