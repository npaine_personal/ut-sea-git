#ifndef _UTILS_H
#define _UTILS_H

#define SHOW_TRANSACTIONS 0 

#define PI 3.14159265

#define USEC_PER_SEC 1000000
#define USEC_PER_MSEC 1000
#define NS_PER_SEC 1000000000

#define ns2sec(ns) ((double)(ns)/(double)NS_PER_SEC)
#define us2sec(us) ((double)(us)/(double)USEC_PER_SEC)
#define sec2us(sec) ((double)(sec)*(double)USEC_PER_SEC)
#define rad2deg(rad) ((float)(rad)*180/PI)
#define deg2rad(deg) ((float)(deg)*PI/180)
#define kg2lbs(kg) ((float)(kg)*2.2046226)
#define lbs2kg(lbs) ((float)(lbs)/2.2046226)
#define radps2rpm(radps) (float)(radps)*30/PI
#define hz2radps(hz) ((float)(hz)*2*PI)
#define radps2hz(radps) ((float)(radps)/2/PI)

//request IO access
void IOPermission(unsigned int abase, unsigned int asize);

//verbose outb
void outbv(int value, int addr);

//verbose outw
void outwv(int value, int addr);

//verbose inb
int inbv(int addr);

//verbose inw
int inwv(int addr);

#endif //_UTILS_H
