#ifndef _CSVPARSER_H
#define _CSVPARSER_H

#include <string>
#include <iostream>
#include <vector>
#include <stdlib.h> 

struct csv_entry{
  std::vector<float> data;
  int idx;
};

//parses filename into vectors containing columns of the csv file
//column indexes should be referenced from 1 not 0
int readCSVFile(std::string filename, int numCols, ...);

#endif //_CSVPARSER_H
