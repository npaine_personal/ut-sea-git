#include "classyFilters.hpp"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

//! @brief Construct a generic IIR Filter
//! @param length History length for filter
//! @param sampleTime_sec Period in seconds at which the filter is invoked
IIR_Filter::IIR_Filter(unsigned length, float sampleTime_sec) {
    inCoeff  = new float[length+1];
    outCoeff = new float[length];
    inPrev   = new float[length];
    outPrev  = new float[length];
    coeffsCalculated = false;
    this->length = length;
    this->sampleTime_sec = sampleTime_sec;
    //printf("iir filter constructor\n");
}

//! @brief Construct a generic IIR Filter
//! @param length History length for filter
//! @param sampleTime_sec Period in seconds at which the filter is invoked
IIR_Filter::IIR_Filter(unsigned length) {
    inCoeff  = new float[length+1];
    outCoeff = new float[length];
    inPrev   = new float[length];
    outPrev  = new float[length];
    coeffsCalculated = false;
    this->length = length;
    //printf("iir filter constructor\n");
}

//! @brief Construct a generic IIR Filter
IIR_Filter::IIR_Filter() {
    inCoeff  = 0;
    outCoeff = 0;
    inPrev   = 0;
    outPrev  = 0;
    coeffsCalculated = false;
    this->length = 0;
    //printf("iir filter constructor\n");
}

IIR_Filter::~IIR_Filter() {
    delete inCoeff;
    delete outCoeff;
    delete inPrev;
    delete outPrev;
}


//! @brief Calculate the next output of the filter given an input
//! @param data_in : incoming filter data
//! @returns filter output
float IIR_Filter::next( float data_in ) {
    unsigned int i;
    float data_out = 0;

    if(coeffsCalculated) {
        // apply filter
        data_out = inCoeff[0] * data_in;
        for( i=0; i<length; i++) {
            data_out += inCoeff[i+1] * inPrev[i];
            data_out += outCoeff[i] * outPrev[i];
        }

        // shift values
        for( i=length-1; i>0; i--) {
            inPrev[i] = inPrev[i-1];
            outPrev[i] = outPrev[i-1];
        }

        inPrev[0] = data_in;
        outPrev[0] = data_out;
        //printf("next calculated\n");
    }

    return data_out; 
}

//! @brief Resets filter data to zero, but does not modify coefficients
void IIR_Filter::reset() {
    unsigned int i;

	for( i=0; i<length; i++) {
		inPrev[i] = 0.0;
		outPrev[i] = 0.0;
	}
}

void IIR_Filter::init( float value ){
	unsigned int i;

	for( i=0; i<length; i++) {
		inPrev[i] = value;
	}
}

bool IIR_Filter::hasValidCoeffs(){
	return coeffsCalculated;
}

void IIR_Filter::printState(void){
    unsigned int i;

    printf("Coeffs are:\n in: ");
    for(i=0; i<=length; i++){
        printf("%f ", inCoeff[i]);
    }
    printf("\nout: ");
    for(i=0; i<length; i++){
        printf("%f ", outCoeff[i]);
    }
    printf("\n");

    printf("Data history is:\n in: ");
    for(i=0; i<length; i++){
        printf("%f ", inPrev[i]);
    }
    printf("\nout: ");
    for(i=0; i<length; i++){
        printf("%f ", outPrev[i]);
    }
    printf("\n");

}

//! @brief calculates IIR filter coeffs for a velocity filter with a nth order LPF given:
//! @param cutoffFreq_hz : filter LPF cutoff frequency in Hz
bool VelocityFilter::calculateCoeffs( float cutoffFreq_hz ) {
    float den, t0;
    unsigned int i;
    bool validConfig = true;
    float w_c = hz2radps(cutoffFreq_hz);
    float t_s = sampleTime_sec;

#if FILTER_DEBUG 
    printf("w_c: %f t_s: %f\n", w_c, t_s);
#endif

    // automatically generated code from gen_VfiltCoeffs.m in sys_id repo
    // use a switch to pick which code to use
    //! @todo generalize coeff generation?
    switch (length) {
    	case 1:
			  t0 = w_c*t_s+2.0;
			  den = t0;
			  t0 = w_c*2.0;
			  inCoeff[0] = t0/den;
			  t0 = w_c*-2.0;
			  inCoeff[1] = t0/den;
			  t0 = -w_c*t_s+2.0; 
			  outCoeff[0] = t0/den;
			  break;
      case 2: 
        t0 = (t_s*t_s)*(w_c*w_c)*2.5E3+t_s*w_c*7.071E3+1.0E4;
        den = t0;
        t0 = t_s*(w_c*w_c)*5.0E3;
        inCoeff[0] = t0/den;
        inCoeff[1] = 0;
        t0 = t_s*(w_c*w_c)*-5.0E3;
        inCoeff[2] = t0/den;
        t0 = (t_s*t_s)*(w_c*w_c)*-5.0E3+2.0E4;
        outCoeff[0] = t0/den;
        t0 = (t_s*t_s)*(w_c*w_c)*-2.5E3+t_s*w_c*7.071E3-1.0E4;
        outCoeff[1] = t0/den;
        break;
      // implement other cases as needed

      // default sets all coeffs to 0
      default:
        for(i = 0; i < length; i++) {
            inCoeff[i]=0;
            outCoeff[i]=0;
        }
        inCoeff[length]=0; // in coeff is a bit longer
        validConfig = false;
        break;
    }

#if FILTER_DEBUG
    printf("Vel coeffs are:\n in: ");
    for(i=0; i<=length; i++){
        printf("%f ", inCoeff[i]);
    }
    printf("\nout: ");
    for(i=0; i<length; i++){
        printf("%f ", outCoeff[i]);
    }
    printf("\n");
#endif

    coeffsCalculated = true;
    return validConfig;
}

//! @brief calculates IIR filter coeffs for an acceleration filter with an nth order LPF given:
//! @param cutoffFreq_hz : filter LPF cutoff frequency in Hz
bool AccelerationFilter::calculateCoeffs( float cutoffFreq_hz ) {
    float den, t0;
    unsigned int i;
    bool validConfig = true;
    float w_c = hz2radps(cutoffFreq_hz);
    float t_s = sampleTime_sec;

    // automatically generated code from gen_VfiltCoeffs.m in sys_id repo
    // use a switch to pick which code to use
    //! @todo generalize coeff generation?
    switch (length) {
        case 3:
			t0 = w_c*t_s*8.0+(w_c*w_c)*(t_s*t_s)*4.0+(w_c*w_c*w_c)*(t_s*t_s*t_s)+8.0;
			den = t0;
			t0 = (w_c*w_c*w_c)*t_s*4.0;
			inCoeff[0] = t0/den;
			t0 = (w_c*w_c*w_c)*t_s*-4.0;
			inCoeff[1] = t0/den;
			t0 = (w_c*w_c*w_c)*t_s*-4.0;
			inCoeff[2] = t0/den;
			t0 = (w_c*w_c*w_c)*t_s*4.0;
			inCoeff[3] = t0/den;
			t0 = w_c*t_s*8.0-(w_c*w_c)*(t_s*t_s)*4.0-(w_c*w_c*w_c)*(t_s*t_s*t_s)*3.0+2.4E1;
			outCoeff[0] = t0/den;
			t0 = w_c*t_s*8.0+(w_c*w_c)*(t_s*t_s)*4.0-(w_c*w_c*w_c)*(t_s*t_s*t_s)*3.0-2.4E1;
			outCoeff[1] = t0/den;
			t0 = w_c*t_s*-8.0+(w_c*w_c)*(t_s*t_s)*4.0-(w_c*w_c*w_c)*(t_s*t_s*t_s)+8.0;
			outCoeff[2] = t0/den;
            break;
        // implement other cases as needed

        // default sets all coeffs to 0
        default:
            for(i = 0; i < length; i++) {
                inCoeff[i]=0;
                outCoeff[i]=0;
            }
            inCoeff[length]=0; // in coeff is a bit longer
            validConfig = false;
            break;
    }

    coeffsCalculated = true;
    return validConfig;
}

//! @brief calculates IIR filter coeffs for an nth order low pass filter given:
//! @param cutoffFreq_hz : filter LPF cutoff frequency in Hz
bool LowPassFilter::calculateCoeffs( float cutoffFreq_hz ) {
    float den, t0;
    unsigned int i; 
    bool validConfig = true;
    float w_c = hz2radps(cutoffFreq_hz);
    float t_s = sampleTime_sec;

#if FILTER_DEBUG 
    printf("w_c: %f t_s: %f\n", w_c, t_s);
#endif

    // automatically generated code from gen_forceControlFilts.m in sys_id repo
    // use a switch to pick which code to use
    //! @todo generalize coeff generation?
    switch (length) {
        case 2:
        	t0 = (t_s*t_s)*(w_c*w_c)*2.5E3+t_s*w_c*7.071E3+1.0E4;
          den = t0;
          t0 = (t_s*t_s)*(w_c*w_c)*2.5E3;
          inCoeff[0] = t0/den;
          t0 = (t_s*t_s)*(w_c*w_c)*5.0E3;
          inCoeff[1] = t0/den;
          t0 = (t_s*t_s)*(w_c*w_c)*2.5E3;
          inCoeff[2] = t0/den;
          t0 = (t_s*t_s)*(w_c*w_c)*-5.0E3+2.0E4;
          outCoeff[0] = t0/den;
          t0 = (t_s*t_s)*(w_c*w_c)*-2.5E3+t_s*w_c*7.071E3-1.0E4;
          outCoeff[1] = t0/den;
          break;
        case 3:
          t0 = w_c*t_s*8.0+(w_c*w_c)*(t_s*t_s)*4.0+(w_c*w_c*w_c)*(t_s*t_s*t_s)+8.0;
          den = t0;
          t0 = (w_c*w_c*w_c)*(t_s*t_s*t_s);
          inCoeff[0] = t0/den;
          t0 = (w_c*w_c*w_c)*(t_s*t_s*t_s)*3.0;
          inCoeff[1] = t0/den;
          t0 = (w_c*w_c*w_c)*(t_s*t_s*t_s)*3.0;
          inCoeff[2] = t0/den;
          t0 = (w_c*w_c*w_c)*(t_s*t_s*t_s);
          inCoeff[3] = t0/den;
          t0 = w_c*t_s*8.0-(w_c*w_c)*(t_s*t_s)*4.0-(w_c*w_c*w_c)*(t_s*t_s*t_s)*3.0+2.4E1;
          outCoeff[0] = t0/den;
          t0 = w_c*t_s*8.0+(w_c*w_c)*(t_s*t_s)*4.0-(w_c*w_c*w_c)*(t_s*t_s*t_s)*3.0-2.4E1;
          outCoeff[1] = t0/den;
          t0 = w_c*t_s*-8.0+(w_c*w_c)*(t_s*t_s)*4.0-(w_c*w_c*w_c)*(t_s*t_s*t_s)+8.0;
          outCoeff[2] = t0/den;
          break;
        case 4:
          t0 = w_c*t_s*1.3066E8+(w_c*w_c)*(t_s*t_s)*8.5357653E7+(w_c*w_c*w_c)*(t_s*t_s*t_s)*3.2665E7+(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*6.25E6+1.0E8;
          den = t0;
          t0 = (w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*6.25E6;
          inCoeff[0] = t0/den;
          t0 = (w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*2.5E7;
          inCoeff[1] = t0/den;
          t0 = (w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*3.75E7;
          inCoeff[2] = t0/den;
          t0 = (w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*2.5E7;
          inCoeff[3] = t0/den;
          t0 = (w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*6.25E6;
          inCoeff[4] = t0/den;
          t0 = w_c*t_s*2.6132E8-(w_c*w_c*w_c)*(t_s*t_s*t_s)*6.533E7-(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*2.5E7+4.0E8;
          outCoeff[0] = t0/den;
          t0 = (w_c*w_c)*(t_s*t_s)*1.70715306E8-(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*3.75E7-6.0E8;
          outCoeff[1] = t0/den;
          t0 = w_c*t_s*-2.6132E8+(w_c*w_c*w_c)*(t_s*t_s*t_s)*6.533E7-(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*2.5E7+4.0E8;
          outCoeff[2] = t0/den;
          t0 = w_c*t_s*1.3066E8-(w_c*w_c)*(t_s*t_s)*8.5357653E7+(w_c*w_c*w_c)*(t_s*t_s*t_s)*3.2665E7-(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*6.25E6-1.0E8;
          outCoeff[3] = t0/den;
          break;
        case 5:
          t0 = w_c*t_s*2.236E6+(w_c*w_c)*(t_s*t_s)*1.808962E6+(w_c*w_c*w_c)*(t_s*t_s*t_s)*9.04481E5+(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*2.795E5+(w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E4+2.0E6;
          den = t0;
          t0 = (w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E4;
          inCoeff[0] = t0/den;
          t0 = (w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*3.125E5;
          inCoeff[1] = t0/den;
          t0 = (w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E5;
          inCoeff[2] = t0/den;
          t0 = (w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E5;
          inCoeff[3] = t0/den;
          t0 = (w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*3.125E5;
          inCoeff[4] = t0/den;
          t0 = (w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E4;
          inCoeff[5] = t0/den;
          t0 = w_c*t_s*6.708E6+(w_c*w_c)*(t_s*t_s)*1.808962E6-(w_c*w_c*w_c)*(t_s*t_s*t_s)*9.04481E5-(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*8.385E5-(w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*3.125E5+1.0E7;
          outCoeff[0] = t0/den;
          t0 = w_c*t_s*-4.472E6+(w_c*w_c)*(t_s*t_s)*3.617924E6+(w_c*w_c*w_c)*(t_s*t_s*t_s)*1.808962E6-(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*5.59E5-(w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E5-2.0E7;
          outCoeff[1] = t0/den;
          t0 = w_c*t_s*-4.472E6-(w_c*w_c)*(t_s*t_s)*3.617924E6+(w_c*w_c*w_c)*(t_s*t_s*t_s)*1.808962E6+(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*5.59E5-(w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E5+2.0E7;
          outCoeff[2] = t0/den;
          t0 = w_c*t_s*6.708E6-(w_c*w_c)*(t_s*t_s)*1.808962E6-(w_c*w_c*w_c)*(t_s*t_s*t_s)*9.04481E5+(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*8.385E5-(w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*3.125E5-1.0E7;
          outCoeff[3] = t0/den;
          t0 = w_c*t_s*-2.236E6+(w_c*w_c)*(t_s*t_s)*1.808962E6-(w_c*w_c*w_c)*(t_s*t_s*t_s)*9.04481E5+(w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s)*2.795E5-(w_c*w_c*w_c*w_c*w_c)*(t_s*t_s*t_s*t_s*t_s)*6.25E4+2.0E6;
          outCoeff[4] = t0/den;
          break;
        // implement other cases as needed

        // default sets all coeffs to 0
        default:
            for(i = 0; i < length; i++) {
                inCoeff[i]=0;
                outCoeff[i]=0;
            }
            inCoeff[length]=0; // in coeff is a bit longer
            validConfig = false;
            break;
    }

#if FILTER_DEBUG
    printf("LPF coeffs are:\n in: ");
    for(i=0; i<=length; i++){
        printf("%f ", inCoeff[i]);
    }
    printf("\nout: ");
    for(i=0; i<length; i++){
        printf("%f ", outCoeff[i]);
    }
    printf("\n");
#endif

    coeffsCalculated = true;
    return validConfig;
}

//! @brief calculates IIR filter coeffs for an inverse MKB filter with pd feedback given with nth order LPF given:
//! @param cutoffFreq_hz : filter LPF cutoff frequency in Hz
bool TorqueControlDOB_Filter::calculateCoeffs( float Q_fc, float m,  float b, float k, float Kp, float Kd, float Kd_fc, float N, float eff, float i2T ) {
    float den, t0;
    unsigned int i;
    bool validConfig = true; 
    float Q_wc = hz2radps(Q_fc);
    float Kd_wc = hz2radps(Kd_fc);
    float t_s = sampleTime_sec;
 
#if FILTER_DEBUG 
    printf("Q_wc: %f t_s: %f\n", Q_wc, t_s);
#endif

    // automatically generated code from gen_forceControlFilts.m
    // use a switch to pick which code to use
    //! @todo generalize coeff generation?
    switch (length) {
        case 4:
          t0 = k*1.6E1+(Q_wc*Q_wc)*k*(t_s*t_s)*8.0+(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*2.0+Kd_wc*k*t_s*8.0+Q_wc*k*t_s*1.6E1+Kd_wc*Q_wc*k*(t_s*t_s)*8.0+Kd_wc*(Q_wc*Q_wc)*k*(t_s*t_s*t_s)*4.0+Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)+Kp*N*eff*i2T*k*1.6E1+Kd*Kd_wc*N*eff*i2T*k*1.6E1+Kp*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s)*8.0+Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0+Kd_wc*Kp*N*eff*i2T*k*t_s*8.0+Kp*N*Q_wc*eff*i2T*k*t_s*1.6E1+Kd*Kd_wc*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s)*8.0+Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0+Kd_wc*Kp*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0+Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)+Kd*Kd_wc*N*Q_wc*eff*i2T*k*t_s*1.6E1+Kd_wc*Kp*N*Q_wc*eff*i2T*k*(t_s*t_s)*8.0;
          den = t0;
          t0 = (Q_wc*Q_wc*Q_wc)*b*(t_s*t_s)*4.0+(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*2.0+(Q_wc*Q_wc*Q_wc)*m*t_s*8.0+Kd_wc*(Q_wc*Q_wc*Q_wc)*b*(t_s*t_s*t_s)*2.0+Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)+Kd_wc*(Q_wc*Q_wc*Q_wc)*m*(t_s*t_s)*4.0+Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0+Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0+Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s);
          inCoeff[0] = t0/den;
          t0 = (Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*4.0-(Q_wc*Q_wc*Q_wc)*m*t_s*1.6E1+Kd_wc*(Q_wc*Q_wc*Q_wc)*b*(t_s*t_s*t_s)*4.0+Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)*4.0+Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0+Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0+Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)*4.0;
          inCoeff[1] = t0/den;
          t0 = (Q_wc*Q_wc*Q_wc)*b*(t_s*t_s)*-8.0+Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)*6.0-Kd_wc*(Q_wc*Q_wc*Q_wc)*m*(t_s*t_s)*8.0+Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)*6.0;
          inCoeff[2] = t0/den;
          t0 = (Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*-4.0+(Q_wc*Q_wc*Q_wc)*m*t_s*1.6E1-Kd_wc*(Q_wc*Q_wc*Q_wc)*b*(t_s*t_s*t_s)*4.0+Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)*4.0-Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0-Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0+Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)*4.0;
          inCoeff[3] = t0/den;
          t0 = (Q_wc*Q_wc*Q_wc)*b*(t_s*t_s)*4.0-(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*2.0-(Q_wc*Q_wc*Q_wc)*m*t_s*8.0-Kd_wc*(Q_wc*Q_wc*Q_wc)*b*(t_s*t_s*t_s)*2.0+Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)+Kd_wc*(Q_wc*Q_wc*Q_wc)*m*(t_s*t_s)*4.0-Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0-Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0+Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s);
          inCoeff[4] = t0/den;
          t0 = k*6.4E1-(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*4.0+Kd_wc*k*t_s*1.6E1+Q_wc*k*t_s*3.2E1-Kd_wc*(Q_wc*Q_wc)*k*(t_s*t_s*t_s)*8.0-Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)*4.0+Kp*N*eff*i2T*k*6.4E1+Kd*Kd_wc*N*eff*i2T*k*6.4E1-Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0+Kd_wc*Kp*N*eff*i2T*k*t_s*1.6E1+Kp*N*Q_wc*eff*i2T*k*t_s*3.2E1-Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0-Kd_wc*Kp*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*8.0-Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)*4.0+Kd*Kd_wc*N*Q_wc*eff*i2T*k*t_s*3.2E1;
          outCoeff[0] = t0/den;
          t0 = k*-9.6E1+(Q_wc*Q_wc)*k*(t_s*t_s)*1.6E1+Kd_wc*Q_wc*k*(t_s*t_s)*1.6E1-Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)*6.0-Kp*N*eff*i2T*k*9.6E1-Kd*Kd_wc*N*eff*i2T*k*9.6E1+Kp*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s)*1.6E1+Kd*Kd_wc*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s)*1.6E1-Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)*6.0+Kd_wc*Kp*N*Q_wc*eff*i2T*k*(t_s*t_s)*1.6E1;
          outCoeff[1] = t0/den;
          t0 = k*6.4E1+(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*4.0-Kd_wc*k*t_s*1.6E1-Q_wc*k*t_s*3.2E1+Kd_wc*(Q_wc*Q_wc)*k*(t_s*t_s*t_s)*8.0-Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)*4.0+Kp*N*eff*i2T*k*6.4E1+Kd*Kd_wc*N*eff*i2T*k*6.4E1+Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0-Kd_wc*Kp*N*eff*i2T*k*t_s*1.6E1-Kp*N*Q_wc*eff*i2T*k*t_s*3.2E1+Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0+Kd_wc*Kp*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*8.0-Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)*4.0-Kd*Kd_wc*N*Q_wc*eff*i2T*k*t_s*3.2E1;
          outCoeff[2] = t0/den;
          t0 = k*-1.6E1-(Q_wc*Q_wc)*k*(t_s*t_s)*8.0+(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s)*2.0+Kd_wc*k*t_s*8.0+Q_wc*k*t_s*1.6E1-Kd_wc*Q_wc*k*(t_s*t_s)*8.0+Kd_wc*(Q_wc*Q_wc)*k*(t_s*t_s*t_s)*4.0-Kd_wc*(Q_wc*Q_wc*Q_wc)*k*(t_s*t_s*t_s*t_s)-Kp*N*eff*i2T*k*1.6E1-Kd*Kd_wc*N*eff*i2T*k*1.6E1-Kp*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s)*8.0+Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0+Kd_wc*Kp*N*eff*i2T*k*t_s*8.0+Kp*N*Q_wc*eff*i2T*k*t_s*1.6E1-Kd*Kd_wc*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s)*8.0+Kd*Kd_wc*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*2.0+Kd_wc*Kp*N*(Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s)*4.0-Kd_wc*Kp*N*(Q_wc*Q_wc*Q_wc)*eff*i2T*k*(t_s*t_s*t_s*t_s)+Kd*Kd_wc*N*Q_wc*eff*i2T*k*t_s*1.6E1-Kd_wc*Kp*N*Q_wc*eff*i2T*k*(t_s*t_s)*8.0;
          outCoeff[3] = t0/den;
          break;
        // implement other cases as needed

        // default sets all coeffs to 0
        default:
            for(i = 0; i < length; i++) {
                inCoeff[i]=0;
                outCoeff[i]=0;
            }
            inCoeff[length]=0; // in coeff is a bit longer
            validConfig = false;
            break;
    }

#if FILTER_DEBUG
    printf("TDOB coeffs are:\n in: ");
    for(i=0; i<=length; i++){
        printf("%f ", inCoeff[i]);
    }
    printf("\nout: ");
    for(i=0; i<length; i++){
        printf("%f ", outCoeff[i]);
    }
    printf("\n");
#endif

    coeffsCalculated = true;
    return validConfig;
}

//! @brief calculates IIR filter coeffs for an nth order low pass filter given:
//! @param cutoffFreq_hz : filter LPF cutoff frequency in Hz
bool MassDamperForce_Filter::calculateCoeffs( float cutoffFreq_hz, float mass, float damping ) {
    float den, t0;
    unsigned int i; 
    bool validConfig = true;
    float Qp_wc = hz2radps(cutoffFreq_hz);
    float t_s = sampleTime_sec;
    float m = mass;
    float b = damping;

#if FILTER_DEBUG 
    printf("Qp_wc: %f t_s: %f\n", Qp_wc, t_s);
#endif

    // automatically generated code from gen_positionControlFilts.m in sys_id repo
    // use a switch to pick which code to use
    //! @todo generalize coeff generation?
    switch (length) {
        case 2:
            t0 = Qp_wc*t_s*7.071E3+(Qp_wc*Qp_wc)*(t_s*t_s)*2.5E3+1.0E4;
            den = t0;
            t0 = (Qp_wc*Qp_wc)*m*1.0E4+(Qp_wc*Qp_wc)*b*t_s*5.0E3;
            inCoeff[0] = t0/den;
            t0 = (Qp_wc*Qp_wc)*m*-2.0E4;
            inCoeff[1] = t0/den;
            t0 = (Qp_wc*Qp_wc)*m*1.0E4-(Qp_wc*Qp_wc)*b*t_s*5.0E3;
            inCoeff[2] = t0/den;
            t0 = (Qp_wc*Qp_wc)*(t_s*t_s)*-5.0E3+2.0E4;
            outCoeff[0] = t0/den;
            t0 = Qp_wc*t_s*7.071E3-(Qp_wc*Qp_wc)*(t_s*t_s)*2.5E3-1.0E4;
            outCoeff[1] = t0/den;
            break;
        // implement other cases as needed

        // default sets all coeffs to 0
        default:
            for(i = 0; i < length; i++) {
                inCoeff[i]=0;
                outCoeff[i]=0;
            }
            inCoeff[length]=0; // in coeff is a bit longer
            validConfig = false;
            break;
    }

#if FILTER_DEBUG
    printf("Pff coeffs are:\n in: ");
    for(i=0; i<=length; i++){
        printf("%f ", inCoeff[i]);
    }
    printf("\nout: ");
    for(i=0; i<length; i++){
        printf("%f ", outCoeff[i]);
    }
    printf("\n");
#endif

    coeffsCalculated = true;
    return validConfig;
}

//! @brief calculates IIR filter coeffs for the position control DOB given:
//! @param cutoffFreq_hz : filter LPF cutoff frequency in Hz ...
bool PositionControlDOB_Filter::calculateCoeffs( float Qp_fc, float Qpd_fc, float m,  float b, float k, float Kp, float Kd, float Kd_fc, float N, float eff, float i2T) {
    float den, t0;
    unsigned int i;
    bool validConfig = true; 
    float Qp_wc = hz2radps(Qp_fc);
    float Qpd_wc = hz2radps(Qpd_fc);
    float Kd_wc = hz2radps(Kd_fc);
    float t_s = sampleTime_sec;
 
#if FILTER_DEBUG 
    printf("Qp_wc: %f Qpd_wc: %f t_s: %f\n", Qp_wc, Qpd_wc, t_s);
#endif

    // automatically generated code from gen_positionControlFilts.m
    // use a switch to pick which code to use
    //! @todo generalize coeff generation?
    switch (length) {
        case 2:
            t0 = (Qp_wc*Qp_wc)*1.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*2.5E3+(Qp_wc*Qp_wc)*Qpd_wc*t_s*7.071E3;
            den = t0;
            t0 = (Qpd_wc*Qpd_wc)*1.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*2.5E3+Qp_wc*(Qpd_wc*Qpd_wc)*t_s*7.071E3;
            inCoeff[0] = t0/den;
            t0 = (Qpd_wc*Qpd_wc)*-2.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*5.0E3;
            inCoeff[1] = t0/den;
            t0 = (Qpd_wc*Qpd_wc)*1.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*2.5E3-Qp_wc*(Qpd_wc*Qpd_wc)*t_s*7.071E3;
            inCoeff[2] = t0/den;
            t0 = (Qp_wc*Qp_wc)*2.0E4-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*5.0E3;
            outCoeff[0] = t0/den;
            t0 = (Qp_wc*Qp_wc)*-1.0E4-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*2.5E3+(Qp_wc*Qp_wc)*Qpd_wc*t_s*7.071E3;
            outCoeff[1] = t0/den;
            break;
        case 3:
            t0 = (Qp_wc*Qp_wc)*2.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*1.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*2.5E3+(Qp_wc*Qp_wc)*Qpd_wc*t_s*2.0E4;
            den = t0;
            t0 = (Qpd_wc*Qpd_wc*Qpd_wc)*t_s*1.0E4+Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s)*7.071E3+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*2.5E3;
            inCoeff[0] = t0/den;
            t0 = (Qpd_wc*Qpd_wc*Qpd_wc)*t_s*-1.0E4+Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s)*7.071E3+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*7.5E3;
            inCoeff[1] = t0/den;
            t0 = (Qpd_wc*Qpd_wc*Qpd_wc)*t_s*-1.0E4-Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s)*7.071E3+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*7.5E3;
            inCoeff[2] = t0/den;
            t0 = (Qpd_wc*Qpd_wc*Qpd_wc)*t_s*1.0E4-Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s)*7.071E3+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*2.5E3;
            inCoeff[3] = t0/den;
            t0 = (Qp_wc*Qp_wc)*6.0E4-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*1.0E4-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*7.5E3+(Qp_wc*Qp_wc)*Qpd_wc*t_s*2.0E4;
            outCoeff[0] = t0/den;
            t0 = (Qp_wc*Qp_wc)*-6.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*1.0E4-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*7.5E3+(Qp_wc*Qp_wc)*Qpd_wc*t_s*2.0E4;
            outCoeff[1] = t0/den;
            t0 = (Qp_wc*Qp_wc)*2.0E4+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*1.0E4-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*2.5E3-(Qp_wc*Qp_wc)*Qpd_wc*t_s*2.0E4;
            outCoeff[2] = t0/den;
            break;
        case 4:
            t0 = (Qp_wc*Qp_wc)*1.0E8+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*8.5357653E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*3.2665E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*6.25E6+(Qp_wc*Qp_wc)*Qpd_wc*t_s*1.3066E8;
            den = t0;
            t0 = (Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s)*2.5E7+Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*1.76775E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*6.25E6;
            inCoeff[0] = t0/den;
            t0 = Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*3.5355E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*2.5E7;
            inCoeff[1] = t0/den;
            t0 = (Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s)*-5.0E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*3.75E7;
            inCoeff[2] = t0/den;
            t0 = Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*-3.5355E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*2.5E7;
            inCoeff[3] = t0/den;
            t0 = (Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s)*2.5E7-Qp_wc*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*1.76775E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*6.25E6;
            inCoeff[4] = t0/den;
            t0 = (Qp_wc*Qp_wc)*4.0E8-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*6.533E7-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*2.5E7+(Qp_wc*Qp_wc)*Qpd_wc*t_s*2.6132E8;
            outCoeff[0] = t0/den;
            t0 = (Qp_wc*Qp_wc)*-6.0E8+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*1.70715306E8-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*3.75E7;
            outCoeff[1] = t0/den;
            t0 = (Qp_wc*Qp_wc)*4.0E8+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*6.533E7-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*2.5E7-(Qp_wc*Qp_wc)*Qpd_wc*t_s*2.6132E8;
            outCoeff[2] = t0/den;
            t0 = (Qp_wc*Qp_wc)*-1.0E8-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc)*(t_s*t_s)*8.5357653E7+(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s)*3.2665E7-(Qp_wc*Qp_wc)*(Qpd_wc*Qpd_wc*Qpd_wc*Qpd_wc)*(t_s*t_s*t_s*t_s)*6.25E6+(Qp_wc*Qp_wc)*Qpd_wc*t_s*1.3066E8;
            outCoeff[3] = t0/den;
            break;
        // implement other cases as needed

        // default sets all coeffs to 0
        default:
            for(i = 0; i < length; i++) {
                inCoeff[i]=0;
                outCoeff[i]=0;
            }
            inCoeff[length]=0; // in coeff is a bit longer
            validConfig = false;
            break;
    }

#if FILTER_DEBUG
    printf("PDOB coeffs are:\n in: ");
    for(i=0; i<=length; i++){
        printf("%f ", inCoeff[i]);
    }
    printf("\nout: ");
    for(i=0; i<length; i++){
        printf("%f ", outCoeff[i]);
    }
    printf("\n");
#endif

    coeffsCalculated = true;
    return validConfig;
}


