
#ifndef IIR_FILTER_HPP
#define IIR_FILTER_HPP

#define FILTER_DEBUG 0

//*base filter class*//

class IIR_Filter {

protected:

    float * inCoeff;  //!< Input coefficients
    float * outCoeff; //!< Output coefficients
    float * inPrev;   //!< Input history
    float * outPrev;  //!< Output history
    bool coeffsCalculated;  //!< flag indicating if we have valid filter coefficients
    unsigned length;  //!< length of filter
    float sampleTime_sec; //filter sample time in units of seconds

public:

    IIR_Filter(unsigned length, float sampleTime_sec);
    IIR_Filter(unsigned length);
    IIR_Filter();
    ~IIR_Filter();

    float next( float data_in ); //calculates next filter output value
    void reset(); //resets filter data to zero, but does not modify coefficients
    void init( float value ); //initializes history of incoming data to value
    bool hasValidCoeffs();
    void setSampleTime(float sampleTime_sec){
    	this->sampleTime_sec = sampleTime_sec;
    }
    void printState(void);

};

//*filter definitions*//

//calculates velocity given position, applies a LPF to the velocity measurement
class VelocityFilter : public IIR_Filter{

public:

	VelocityFilter(unsigned length, float sampleTime_sec) : IIR_Filter(length, sampleTime_sec) {}
	VelocityFilter(unsigned length) : IIR_Filter(length) {}
  VelocityFilter() {}

	//calculate veloctiy filter coeffs given:
	// cutoffFreq_hz : cutoff frequency [Hz]
	bool calculateCoeffs( float cutoffFreq_hz );

	VelocityFilter(unsigned length, float sampleTime_sec, float cutoffFreq_hz) : IIR_Filter(length, sampleTime_sec) {
		calculateCoeffs( cutoffFreq_hz);
	}
};

//calculates acceleration given position, applies a LPF to the acceleration measurement
class AccelerationFilter : public IIR_Filter{

public:

	AccelerationFilter(unsigned length, float sampleTime_sec) : IIR_Filter(length, sampleTime_sec) {}
	AccelerationFilter(unsigned length) : IIR_Filter(length) {}
  AccelerationFilter() {}

	//calculate veloctiy filter coeffs given:
	// cutoffFreq_hz : cutoff frequency [Hz]
	bool calculateCoeffs( float cutoffFreq_hz );

	AccelerationFilter(unsigned length, float sampleTime_sec, float cutoffFreq_hz) : IIR_Filter(length, sampleTime_sec) {
		calculateCoeffs( cutoffFreq_hz);
	}
};

//applies a low pass filter to an input signal
class LowPassFilter : public IIR_Filter{

public:

	LowPassFilter(unsigned length, float sampleTime_sec) : IIR_Filter(length, sampleTime_sec) {}
	LowPassFilter(unsigned length) : IIR_Filter(length) {}
  LowPassFilter() {}

	//calculate LPF filter coeffs given:
	// cutoffFreq_hz : cutoff frequency [Hz]
	bool calculateCoeffs( float cutoffFreq_hz );

	LowPassFilter(unsigned length, float sampleTime_sec, float cutoffFreq_hz) : IIR_Filter(length, sampleTime_sec) {
		calculateCoeffs( cutoffFreq_hz);
	}
};

//calculates applied force given measured spring force for an MKB system with feedback
//used in torque control disturbance observer and dynamic feedforward compensation
class TorqueControlDOB_Filter : public IIR_Filter{

public:

	TorqueControlDOB_Filter(unsigned length, float sampleTime_sec) : IIR_Filter(length, sampleTime_sec) {}
	TorqueControlDOB_Filter(unsigned length) : IIR_Filter(length) {}
  TorqueControlDOB_Filter() {}

	//calculate TDOB filter coeffs given:
	// cutoffFreq_hz : cutoff frequency [Hz]
	// m : effective MKB mass
	// b : effective MKB damping
	// k : effective MKB stiffness
	bool calculateCoeffs( float Q_fc, float m,  float b, float k, float Kp, float Kd, float Kd_wc, float N, float eff, float i2T);

	TorqueControlDOB_Filter(unsigned length, float sampleTime_sec, float Q_fc, float m,  float b, float k, float Kp, float Kd, float Kd_wc, float N, float eff, float i2T)
	  : IIR_Filter(length, sampleTime_sec) {
		calculateCoeffs( Q_fc, m,  b, k, Kp, Kd, Kd_wc, N, eff, i2T);
	}
};

//calculates force given a desired position and a second order plant model (mass and damper)
class MassDamperForce_Filter : public IIR_Filter{

public:

	MassDamperForce_Filter(unsigned length, float sampleTime_sec) : IIR_Filter(length, sampleTime_sec) {}
	MassDamperForce_Filter(unsigned length) : IIR_Filter(length) {}
  MassDamperForce_Filter() {}

	//calculate LPF filter coeffs given:
	// cutoffFreq_hz : cutoff frequency [Hz]
	bool calculateCoeffs( float cutoffFreq_hz, float mass, float damping );

	MassDamperForce_Filter(unsigned length, float sampleTime_sec, float cutoffFreq_hz, float mass, float damping) : IIR_Filter(length, sampleTime_sec) {
		calculateCoeffs( cutoffFreq_hz, mass, damping);
	}
};

//calculates desired position given measured position and a funky (5th order) plant model
//used in position control disturbance observer
class PositionControlDOB_Filter : public IIR_Filter{

public:

	PositionControlDOB_Filter(unsigned length, float sampleTime_sec) : IIR_Filter(length, sampleTime_sec) {}
	PositionControlDOB_Filter(unsigned length) : IIR_Filter(length) {}
  PositionControlDOB_Filter() {}

	//calculate TDOB filter coeffs given:
	// cutoffFreq_hz : cutoff frequency [Hz]
	// m : effective MKB mass
	// b : effective MKB damping
	// k : effective MKB stiffness
	bool calculateCoeffs( float Qp_fc, float Qpd_fc, float m,  float b, float k, float Kp, float Kd, float Kd_fc, float N, float eff, float i2T);

	PositionControlDOB_Filter(unsigned length, float sampleTime_sec, float Qp_fc, float Qpd_fc, float m,  float b, float k, float Kp, float Kd, float Kd_fc, float N, float eff, float i2T)
	  : IIR_Filter(length, sampleTime_sec) {
		calculateCoeffs( Qp_fc, Qpd_fc, m,  b, k, Kp, Kd, Kd_fc, N, eff, i2T);
	}
};

#endif //IIR_FILTER_HPP
