//starts the RTAI timer

#include <stdio.h>
#include <stdlib.h> 
#include <rtai_shm.h>
#include <rtai_fifos.h>

int main(int argc, char **argv) {

   if ( rt_is_hard_timer_running() ){
    printf("timer is running\n");
   }
   else {
    printf("timer is not running\n");
   }

   return 0;
}



