//starts the RTAI timer

#include <stdio.h>
#include <stdlib.h> 
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include "utils.h"

#define TIMER_FREQ_HZ 2000
#define TIMER_PERIOD_SEC (1.0/(float)TIMER_FREQ_HZ)

int main(int argc, char **argv) {

   printf("starting timer\n");
   rt_set_periodic_mode();
   start_rt_timer(nano2count(NS_PER_SEC * TIMER_PERIOD_SEC));

   return 0;
}



