This folder houses all of the code used to run the UT-SEA.  Note that the word `UT-SEA' here refers to both the compliant and rigid actuators on the UT-SEA v2 testbench. Here's a quick overview of the files:

actuator.* - This is the c++ actuator class, used to instantiate an actuator object which can be either the compliant or the rigid actuator (assuming you're using the UT-SEA v2 testbench)

actuator.cpp - contains the actuator objects as well as all of the control code implementation for both rigid and compliant actuators

actuator.hpp - This is where controller parameters are tuned and where pieces of each controller are enabled/disabled (DOB for instance)

actuator_params.h - This file contains all of the system parameters used in the UT-SEA testbench (link lengths, mass properties, etc.).  It also contains all unit conversion macros.

actuator_utils.* - Low level actuator utilities

data_logger.cpp - This is the program used to record data from shared memory to file.  It is necessary to run this program while the actuator test program (discussed below) is running to save data.

measure.cpp - This program is helpful to run before actually running a test on the UT-SEA.  Its main use is to check that the measured positions and forces are what they are supposed to be before powering on the actuator.

tuning_params.txt - This file serves as a repository for the commands issued to run each type of test.  It is helpful to be able to refer to this file to recreate a test from the past.

test_* - These are the programs which actually perform the various tasks using the UT-SEA.  These tasks are outlined below:

	cresponse - Current chirp
	
	fcontrol - Put the UT-SEA in force control mode using the force controller described in Nicholas Paine's Ph.D Thesis (Fig 4.3c). Various signal profiles are possible.
	
	filters - Check the filters used in all of the UT-SEA's control code and save their output in a file.  This is extremely useful when debugging control issues to ensure the implemented filters match their simulated counterparts.
	
	fresponse - Put the UT-SEA in force control mode and track a chirp signal.  Useful for measuring force-tracking bandwidth to making bode plots.

	impedance - Put the UT-SEA in impedance control mode.  This mode is defined by an outer position feedback loop (a PD controller) and an inner force feedback loop (same force controller as fcontrol and fresponse). Various signal profiles are possible.  Can emulate user-specified feedback delays.

	motorImpedance - Same as `impedance' but is intended for standalone motor tests.  The inner force loop in this case is just current control (see actuator.cpp).

	test_pcontrol - Put the UT-SEA in position control mode using the position controller described in Nicholas Paine's Ph.D Thesis (Fig 4.8). Various signal profiles are possible.  Not yet tested on the rigid actuator.

	test_presponse - Put the UT-SEA in position control mode and track a chirp signal.

