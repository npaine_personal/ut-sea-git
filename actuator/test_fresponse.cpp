//sends chirp signal as desired actuator force and measures force response
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include <referenceSignals.h>
#include "actuator.hpp"
#include "actuator_utils.h"

#define RTAI_TASK_NAME "FRESP" //must be unique

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define TIME_TO_BEGIN 10 //wait this long before starting test

//reference params

static float Switching_amp_n = F_SETPOINT_FORCE_AMPLITUDE_N_DEFAULT;
static float Sweep_offset_n = F_SETPOINT_MID_FORCE_N;
static float Sweep_freqLow_hz = F_SWEEP_FREQ_HZ_LOW;
static float Sweep_freqHigh_hz = F_SWEEP_FREQ_HZ_HIGH;
static float Sweep_rate = F_SWEEP_RATE;

static char ActuatorSel = ACTUATOR1_MASK;
static char Safeness=SAFE;

static unsigned char End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -a <amp (nm)>  : setpoint switching amplitude (default: %f)\n", Switching_amp_n);
  printf ("  -q <freq (hz)> : setpoint switching frequency (default: %f)\n", Sweep_freqHigh_hz);
  printf ("  -t <actuator>  : select actuator, 1=actuator1, 2=actuator2, 3=both (default: %d)\n", ActuatorSel);
  printf ("  -u             : unsafe, disables position and force limits, don't use with motor power on (default: %d)\n", !(Safeness && Safeness));
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "a:t:q:ush?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'a': 
          Switching_amp_n = atof(optarg);
          break;
       case 'u':
          Safeness = UNSAFE;
          break;
      case 't':
          ActuatorSel = atoi(optarg);
          break;
      case 'q': 
          Sweep_freqHigh_hz = atof(optarg);
          break;
      case '?': // help
      case 'h': // help
      case ':':
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

int main(int argc, char **argv) {

   int timer_already_running;
   float startCountDown=TIME_TO_BEGIN;

   float desiredForce_n;

   SEA_Actuator actuator1;
   Actuator_state_str *ass;
   unsigned int rtf_fd;

   Rigid_Actuator actuator2;
   Actuator_state_str *ass2;
   unsigned int rtf_fd2;

   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;
 
   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv); //check command line params

   ass = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd,1);

   ass2 = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS2), sizeof(Actuator_state_str));
   rtf_fd2 = open(ASS_FIFO2, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd2,1);

   //print message to console
   if(Safeness){
     printf("Position and force limits enabled: Fmax=%.1fN, TrqMax=%.1fNm\n", (float)MAX_FORCE_N, (float)MAX_TORQUE_NM);
   }
   else{
     printf("Position and force limits disabled\n");
   }

   printf("Controlling force at %d hz with setpoint freq=%.3f amp=%.3f (%.2flbs)\n", (int)CONTROL_FREQ_HZ, Sweep_freqHigh_hz, Switching_amp_n, kg2lbs(ADDED_MASS));

   if(ActuatorSel & ACTUATOR1_MASK) {
     printf("PD1 enabled: %d\n", FCONTROL1_EN_PD);
     printf("DOB1 enabled: %d\n", FCONTROL1_EN_DOB);
     printf("Kp=%.3f Kd=%.5f Fkd_FC=%.3f Q_FC=%.3f\n", (float)FCONTROL1_KP, (float)FCONTROL1_KD, (float)FCONTROL1_KD_FC, (float)FCONTROL1_Q_FC);
   }

   if(ActuatorSel & ACTUATOR2_MASK) {
     printf("PD2 enabled: %d\n", FCONTROL2_EN_PD);
     printf("DOB2 enabled: %d\n", FCONTROL2_EN_DOB);
     printf("Kp=%.3f Kd=%.5f Fkd_FC=%.3f Q_FC=%.3f\n", (float)FCONTROL2_KP, (float)FCONTROL2_KD, (float)FCONTROL2_KD_FC, (float)FCONTROL2_Q_FC);
   }

   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   AnalogIn_Init();
   Quad_init(512, 20000, 500, 500, 500, 16384, 500, 500);
   //Quad_resetCounters();
   //quadGetOffset(); //issue with resetting spring incremental encoder, do it this way
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   setMotorCurrents(0.0, 0.0);
   AnalogOut_Enable(); //enables analog outputs
#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif
   
   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }

   //timer initialization
   if ( (timer_already_running = rt_is_hard_timer_running()) ){
    printf("timer is already running\n");
    period = nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC);
   }
   else {
    printf("timer is not already running, starting timer\n");
    rt_set_periodic_mode();
    period = start_rt_timer(nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC));
   }

  //wait to begin
   printf("waiting %d seconds to begin..go hold the arm\n", TIME_TO_BEGIN);

   while(startCountDown > 0.0 && !End){
     printf("%0.0f seconds to start\n",startCountDown);
     startCountDown -= 1.0; 
     sleep(1);
   }
   printf("starting...\n");

   fflush(stdout);

   //keep program from paging
   mlockall(MCL_CURRENT | MCL_FUTURE); 
   rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;

   while(!End){

     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);
     //printf("sample period %lf\n", samplePeriod_s);
     if(samplePeriod_s > 0.002) printf(":( %f\t%f\n", samplePeriod_s, elapsedTime_s); //check for jitter

     //printf("elapsedTime:%f\n", elapsedTime_s);
     actuator1.setSampleTime(elapsedTime_s, samplePeriod_s);
     actuator2.setSampleTime(elapsedTime_s, samplePeriod_s);

     //read actuator state and put in local struct
     if(ActuatorSel & ACTUATOR1_MASK) {
       if(actuator1.updateState(Safeness)){
         End = 1;
         break;
       }

       desiredForce_n = getChirpSignal(elapsedTime_s, samplePeriod_s, Switching_amp_n, Sweep_offset_n, Sweep_freqLow_hz, Sweep_freqHigh_hz, Sweep_rate, &End);
       
       actuator1.servoForce(desiredForce_n);

       setMotor1Current(actuator1.getDesiredMotorCurrent());

       //write actuator state to shared mem
       ass_writeShm(rtf_fd, ass, actuator1.getState());
     }

     if(ActuatorSel & ACTUATOR2_MASK) {
       if(actuator2.updateState(Safeness)){
         End = 1;
         break;
       }

       desiredForce_n = getChirpSignal(elapsedTime_s, samplePeriod_s, Switching_amp_n, Sweep_offset_n, Sweep_freqLow_hz, Sweep_freqHigh_hz, Sweep_rate, &End);

       actuator2.servoForce(desiredForce_n);

       setMotor2Current(actuator2.getDesiredMotorCurrent());

       //write actuator state to shared mem
       ass_writeShm(rtf_fd2, ass2, actuator2.getState());
     }
     
     //printf("wrote to shm at t=%f\n",elapsedTime_s);
     //fflush(stdout);

#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     rt_task_wait_period();
   }

   rt_make_soft_real_time();
   if ( !timer_already_running ){
    printf("stopping timer\n");
    stop_rt_timer();
   }
   rt_task_delete(task);
   rtai_free(nam2num(SHMNAM_ASS), ass);
   rtai_free(nam2num(SHMNAM_ASS2), ass2);
   close(rtf_fd);
   close(rtf_fd2);
   printf("ending...\n");
   setMotorCurrents(0.0, 0.0);
   //AnalogOut_Disable();

   return 0;
}



