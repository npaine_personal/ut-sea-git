//impedance control
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <digital_out.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include <referenceSignals.h>
#include "actuator.hpp"
#include "actuator_utils.h"

#define RTAI_TASK_NAME "MICNTRL" //must be unique

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define TIME_TO_BEGIN 10 //wait this long before starting test

//reference params
static float Switching_freq_hz = P_SETPOINT_SWITCH_FREQ_HZ_DEFAULT;
static float Switching_amp_rad = rot2rad(20);
static float Chirp_upper_freq_hz = P_SWEEP_FREQ_HZ_HIGH;
static float Chirp_rate = P_SWEEP_RATE;
static unsigned char Mode = SINE;
static float Stiffness=0;
static float Damping=0;
static char Safeness=UNSAFE;
static float Switching_offset_rad=0.0;

static unsigned char End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf (" Reference signal options:\n");
  printf ("  Signal type options (choose one, sine wave is default):\n");
  printf ("  -s                      : squarewave setpoint (default: %d)\n", 0);
  printf ("  -c                      : constant (default: %d)\n", 0);
  printf ("  -p                      : chirp (default: %d)\n", 0);
  printf ("  Generic signal options:\n");
  printf ("  -a <amp (rot)>          : setpoint switching amplitude (default: %f)\n", rad2rot(Switching_amp_rad));
  printf ("  -o <offset (rot)>       : setpoint switching offset (default: %f)\n", Switching_offset_rad);
  printf ("  Periodic signal options:\n");
  printf ("  -f <freq (hz)>          : setpoint switching frequency (default: %f)\n", Switching_freq_hz);
  printf ("  Chirp signal options:\n");
  printf ("  -q <freq (hz)>          : chirp high frequency (default: %f)\n", Chirp_upper_freq_hz);
  printf ("  -r <rate>               : chirp rate (default: %f)\n", Chirp_rate);
  printf ("\n Actuator impedance options:\n");
  printf ("  -k <stiffness (Nm/rad)> : setpoint switching amplitude (default: %f)\n", Stiffness);
  printf ("  -b <damping (Nms/rad)>  : setpoint switching amplitude (default: %f)\n", Damping);
  //printf ("\n Actuator selection and safety options:\n");
  //printf ("  -t <actuator>        : select actuator, 1=actuator1, 2=actuator2, 3=both (default: %d)\n", ActuatorSel);
  //printf ("  -u                   : unsafe, disables position and force limits, don't use with motor power on (default: %d)\n", !(Safeness && Safeness));
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "a:f:t:k:b:r:q:o:scpuh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'f': 
          Switching_freq_hz = atof(optarg);
          break;
      case 'a': 
          Switching_amp_rad = rot2rad(atof(optarg));
          break;
      case 'o': 
          Switching_offset_rad = rot2rad(atof(optarg));
          break;
      case 'k': 
          Stiffness = atof(optarg);
          break;
      case 'b': 
          Damping = atof(optarg);
          break;
      case 'r': 
          Chirp_rate = atof(optarg);
          break;
      case 'q': 
          Chirp_upper_freq_hz = atof(optarg);
          break;
      case 's': 
          Mode = SQUARE;
          break;
      case 'c':
          Mode = CONST;
          break;
      case 'p':
          Mode = CHIRP;
          break;
   /*   case 'u':
          Safeness = UNSAFE;
          break;
      case 't':
          ActuatorSel = atoi(optarg);
          break; */
      case '?': // help
      case 'h': // help
      case ':':
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

int main(int argc, char **argv) {

   int timer_already_running;

   float startCountDown=TIME_TO_BEGIN;

   float desiredPosition_rad, desiredVelocity_radps;

   Motor actuator2;
   Actuator_state_str *ass2;
   unsigned int rtf_fd2;

   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;
 
   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv); //check command line params

   ass2 = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS2), sizeof(Actuator_state_str));
   rtf_fd2 = open(ASS_FIFO2, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd2,1);

   //mode-specific config

   //safety
   if(Safeness){
     printf("Position and force limits enabled: Fmax=%.1fN, TrqMax=%.1fNm\n", (float)MAX_FORCE_N, (float)MAX_TORQUE_NM);
   }
   else{
     printf("Position and force limits disabled\n");
   }

   //mode
   printf("Controlling impedance at %d hz with stiffness=%.1f damping=%.1f and ", (int)CONTROL_FREQ_HZ, Stiffness, Damping);
   if(Mode == CONST){
     printf("constant position amp=%.3f\n", Switching_amp_rad);
   }
   else if(Mode == CHIRP){
     Switching_freq_hz = Chirp_upper_freq_hz;
     printf("chirp amp=%.3f max_freq=%.3f rate=%.3f\n", Switching_amp_rad, Switching_freq_hz, Chirp_rate);
   }
   else if(Mode == SQUARE){
     printf("square setpoint amp=%.3f freq=%.3f\n", Switching_amp_rad, Switching_freq_hz);
   }
   else{
     printf("sine setpoint amp=%.3f freq=%.3f\n", Switching_amp_rad, Switching_freq_hz);
   }

   printf("V_fc=%.1f\n", (float)VEL_MOTOR_FC);

   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   AnalogIn_Init();
   Quad_init(512, 20000, 500, 500, 500, 16384, 500, 500);
   //Quad_resetCounters();
   //quadGetOffset(); //issue with resetting spring incremental encoder, do it this way
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   setMotorCurrents(0.0, 0.0);
   AnalogOut_Enable(); //enables analog outputs
#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif
   
   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }

   //timer initialization
   if ( (timer_already_running = rt_is_hard_timer_running()) ){
    printf("timer is already running\n");
    period = nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC);
   }
   else {
    printf("timer is not already running, starting timer\n");
    rt_set_periodic_mode();
    period = start_rt_timer(nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC));
   }

   //wait to begin
   printf("waiting %d seconds to begin..\n", TIME_TO_BEGIN);

   while(startCountDown > 0.0 && !End){
     printf("%0.0f seconds to start\n",startCountDown);
     startCountDown -= 1.0; 
     sleep(1);
   }
   
    //keep program from paging
   mlockall(MCL_CURRENT | MCL_FUTURE); 
   rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   printf("starting...\n");
   fflush(stdout);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;

   while(!End){

     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);
     //printf("sample period %lf\n", samplePeriod_s);
     if(samplePeriod_s > 0.002) printf(":( %f\t%f\n", samplePeriod_s, elapsedTime_s); //check for jitter

     //printf("elapsedTime:%f\n", elapsedTime_s);
     actuator2.setSampleTime(elapsedTime_s, samplePeriod_s);

     if(actuator2.updateState(Safeness)){
       End = 1;
       break;
     }

     getDesiredPosition(&desiredPosition_rad, &desiredVelocity_radps, elapsedTime_s, actuator2.getMotorAngle(), Switching_offset_rad, Switching_amp_rad, Switching_freq_hz, P_SWEEP_RATE, Mode, true, &End);

     actuator2.servoImpedance(desiredPosition_rad, desiredVelocity_radps, 0.0, Stiffness, Damping);

     setMotor2Current(actuator2.getDesiredMotorCurrent());

     //write actuator state to shared mem
     ass_writeShm(rtf_fd2, ass2, actuator2.getState());
     
     
     //printf("wrote to shm at t=%f\n",elapsedTime_s);
     //fflush(stdout);

#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     rt_task_wait_period();
   }

   rt_make_soft_real_time();
   if ( !timer_already_running ){
    printf("stopping timer\n");
    stop_rt_timer();
   }
   rt_task_delete(task);
   rtai_free(nam2num(SHMNAM_ASS2), ass2);
   close(rtf_fd2);
   printf("ending...\n");
   setMotorCurrents(0.0, 0.0);
   //AnalogOut_Disable();
   

   return 0;
}



