
#ifndef ACTUATOR_HPP
#define ACTUATOR_HPP

#include <classyFilters.hpp>
#include <string>
#include <queue>

#define ACTUATOR_DEBUG 1

//state filter parameters
#define VEL_MOTOR_FC 50
#define VEL_ACTUATOR_FC 50
#define VEL_ARM_FC 50

#define CONTROL_FREQ_HZ 1000.0
#define CONTROL_PERIOD_SEC (1.0/(float)CONTROL_FREQ_HZ)

//control parameters
  //SEA
#define FCONTROL1_EN_DOB 1
#define FCONTROL1_EN_PD 1
#define FCONTROL1_DR 0.9
#define FCONTROL1_KP 0.05
#define FCONTROL1_KD ((FCONTROL1_DR*2*sqrt(M_FIT*SPRING_CONST_NpM*(1+FCONTROL1_KP*motorCurrent2BsForce(1)))-B_FIT)*BsForce2motorCurrent(1)/SPRING_CONST_NpM)
#define FCONTROL1_Q_FC 50
#define FCONTROL1_KD_FC 100

#define PCONTROL1_EN_DOB 1
#define PCONTROL1_FC 10 //hz
#define PCONTROL1_DOB_FC 5 //hz

  //Rigid
#define FCONTROL2_EN_DOB 0
#define FCONTROL2_EN_PD 1
#define FCONTROL2_DR 0.95
#define FCONTROL2_KP 0.01
#define FCONTROL2_KD 0
#define FCONTROL2_Q_FC 30
#define FCONTROL2_KD_FC 100

#define PCONTROL2_EN_DOB 0
#define PCONTROL2_FC 10 //hz
#define PCONTROL2_DOB_FC 9 //hz

//filter params
#define FCONTROL1_Q_FILT_LEN 3
#define FCONTROL1_TDOB_FILT_LEN 4
#define PCONTROL1_QP_FILT_LEN 2
#define PCONTROL1_PDOB_FILT_LEN 2

#define FCONTROL2_Q_FILT_LEN 2
#define PCONTROL2_QP_FILT_LEN 2
#define PCONTROL2_PDOB_FILT_LEN 2

typedef struct{
  //timing info
  float SampleTime_s;
  float SamplePeriod_s;

  //actuator state
    //kinematics
  float Motor_angle_rad;
  float Motor_vel_radps;
  float Actuator_pos_m;
  float Actuator_vel_mps;
  float Arm_ang_rad;
  float Arm_vel_radps;

    //dynamics
  float Actuator_force_n;
  float LoadCell_force_n;
  float Arm_torque_nm;

    //energetics
  float Motor_elec_power_w;
  float Motor_mech_power_w;
  float Arm_power_w;
  float Mech_efficiency;
  float Motor_efficiency;
  float Total_efficiency;

  //desired control values
  float Motor_current_des_amps;
  float Actuator_force_des_n;
  float Actuator_pos_des_m;
  float Actuator_vel_des_mps;
  float Arm_ang_des_rad;
  float Arm_vel_des_radps;

  //processing flag
  unsigned char newData;
} Actuator_state_str;

//*base class*//

class Actuator {

protected:

    //state data
    Actuator_state_str State;

    //state filters
    VelocityFilter Vmot_filt; //motor velocity filter
    VelocityFilter Vact_filt; //actuator velocity filter
    VelocityFilter Varm_filt; //output arm velocity filter
    
    //force control filters
    VelocityFilter Fkd_filt;
    
    //position control filters
    MassDamperForce_Filter Pff_filt;
    LowPassFilter Qpd_filt;
    PositionControlDOB_Filter PDOB_filt;

    std::string ActuatorType;
    bool FirstRun_State;
    bool FirstRun_FControl;
    bool FirstRun_ImpControl;
    bool FirstRun_PosControl;
    float Force_ref_prev;
    float Init_bs_pos_m;
    float Init_arm_ang_rad;
    float PosCntrl_arm_ang_offset_rad;
    float PosCntrl_arm_ang_des_prev;

public:

    Actuator(void);
    Actuator(int Qpd_len, int PDOB_len);
    ~Actuator(void){}

    //getters and setters
    void setSampleTime(float sampleTime_s, float samplePeriod_s ){
        State.SampleTime_s = sampleTime_s;
        State.SamplePeriod_s = samplePeriod_s;
    }

    void setMotorCurrent(float current);
    float getMotorAngle(void){ return State.Motor_angle_rad; }
    float getActuatorPos(void){ return State.Actuator_pos_m; }
    float getArmAngle(void){ return State.Arm_ang_rad; }
    float getForce(void){ return State.Actuator_force_n; }
    float getLoadcellForce(void){ return State.LoadCell_force_n; }
    float getDesiredMotorCurrent(void){ return State.Motor_current_des_amps; }
    Actuator_state_str * getState(void){ return &State; }
    void printType(void);

    //state funcitons
    virtual void grabStateData() = 0;
    int updateState(int checkSafety);
    void initFilters(float samplePeriod_sec);
    virtual void sensorInit(void) = 0;

    //control functions
    virtual void servoForce(float desiredForce) = 0;
    void servoImpedance(float posDes, float velDes, float effortDes, float stiffness, float damping);
    void servoPosition(float posDes, float velDes);
    void servoCurrent(float currentDes){
      State.Motor_current_des_amps = currentDes;
    }

    //latency simulation
    int posQueueSize;
    int velQueueSize;
    std::queue<float> posQueue;
    std::queue<float> velQueue;
    void fillPosQueue(float value);
    void fillVelQueue(float value);

};

class SEA_Actuator : public Actuator{

protected:
    TorqueControlDOB_Filter DOB_filt;
    LowPassFilter Q_filt;
    float Init_spring_pos_mm;

public:

    SEA_Actuator(void);

    void grabStateData();
    void initFilters(float samplePeriod_sec);
    void servoForce(float desiredForce);
    void sensorInit(void);

};

class Rigid_Actuator : public Actuator{

protected:
    LowPassFilter Q_filt;

public:

    Rigid_Actuator(void);

    void grabStateData();
    void initFilters(float samplePeriod_sec);
    void servoForce(float desiredForce);
    void sensorInit(void);

};

class Motor : public Actuator{

protected:
    LowPassFilter Q_filt;

public:

    Motor(void);

    void grabStateData();
    void initFilters(float samplePeriod_sec);
    void servoForce(float desiredForce);
    void sensorInit(void);

};

#endif //_ACTUATOR_HPP
