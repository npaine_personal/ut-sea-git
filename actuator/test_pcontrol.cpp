//model based positon (angle) control with disturbance observer
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include <referenceSignals.h>
#include "actuator.hpp"
#include "actuator_utils.h"

#define RTAI_TASK_NAME "PCNTRL" //must be unique

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define TIME_TO_BEGIN 10 //wait this long before starting test

//reference params
static float Switching_freq_hz = P_SETPOINT_SWITCH_FREQ_HZ_DEFAULT;
static float Switching_amp_deg = P_SETPOINT_POS_AMPLITUDE_DEG_DEFAULT;
static float Switching_offset_deg = P_SETPOINT_OFFSET_POS_DEG;
static unsigned char Mode = SINE;
static char ActuatorSel = ACTUATOR1_MASK;
static char Safeness=SAFE;

static unsigned char End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -f <freq (hz)> : setpoint switching frequency (default: %f)\n", P_SETPOINT_SWITCH_FREQ_HZ_DEFAULT);
  printf ("  -a <amp (deg)> : setpoint switching amplitude (default: %f)\n", P_SETPOINT_POS_AMPLITUDE_DEG_DEFAULT);
  printf ("  -i             : spline setpoint (default: %d)\n", 0);
  printf ("  -t <actuator>  : select actuator, 1=actuator1, 2=actuator2, 3=both (default: %d)\n", ActuatorSel);
  printf ("  -u             : unsafe, disables position and force limits, don't use with motor power on (default: %d)\n", !(Safeness && Safeness));
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "f:a:o:t:uih?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'f': 
          Switching_freq_hz = atof(optarg);
          break;
      case 'a': 
          Switching_amp_deg = atof(optarg);
          break;
      case 'o': 
          Switching_offset_deg = atof(optarg);
          break;
      case 'i': 
          Mode = SPLINE;
          break;
      case 'u':
          Safeness = UNSAFE;
          break;
      case 't':
          ActuatorSel = atoi(optarg);
          break;
      case '?': // help
      case 'h': // help
      case ':':
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

int main(int argc, char **argv) {

   int timer_already_running;
   float startCountDown=TIME_TO_BEGIN;

   float desiredForce_n;
   float desiredPosition_rad, desiredVelocity_radps;

   SEA_Actuator actuator1;
   Actuator_state_str *ass;
   unsigned int rtf_fd;

   Rigid_Actuator actuator2;
   Actuator_state_str *ass2;
   unsigned int rtf_fd2;

   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;
 
   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv); //check command line params

   ass = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd,1);

   ass2 = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS2), sizeof(Actuator_state_str));
   rtf_fd2 = open(ASS_FIFO2, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd2,1);

   //print message to console
   if(Safeness){
     printf("Position and force limits enabled: Fmax=%.1fN, TrqMax=%.1fNm\n", (float)MAX_FORCE_N, (float)MAX_TORQUE_NM);
   }
   else{
     printf("Position and force limits disabled\n");
   }

   printf("Controlling position at %d hz with ", (int)CONTROL_FREQ_HZ);
   if(Mode == SPLINE){
     printf("spline setpoint");
 
   }
   else{
     printf("sine setpoint");
   }
   printf(" amp=%.3f freq=%.3f (%.2flbs, J=%0.3f)\n", Switching_amp_deg, Switching_freq_hz, kg2lbs(ADDED_MASS), ARM_INERTIA);

   if(ActuatorSel & ACTUATOR1_MASK) {
     printf("Position params:\n");
     printf("  DOB1 enabled: %d\n", PCONTROL1_EN_DOB);
     printf("  PFF_FC=%.2f QP_FC=%.2f\n", (float)PCONTROL1_FC, (float)PCONTROL1_DOB_FC);
     printf("Force params:\n");
     printf("  PD1 enabled: %d\n", FCONTROL1_EN_PD);
     printf("  DOB1 enabled: %d\n", FCONTROL1_EN_DOB);
     printf("  Kp=%.3f Kd=%.5f Fkd_FC=%.3f Q_FC=%.3f\n", (float)FCONTROL1_KP, (float)FCONTROL1_KD, (float)FCONTROL1_KD_FC, (float)FCONTROL1_Q_FC);
   }

   if(ActuatorSel & ACTUATOR2_MASK) {
     printf("Position params:\n");
     printf("  DOB2 enabled: %d\n", PCONTROL2_EN_DOB);
     printf("  PFF_FC=%.2f QP_FC=%.2f\n", (float)PCONTROL2_FC, (float)PCONTROL2_DOB_FC);
     printf("Force params:\n");
     printf("  PD2 enabled: %d\n", FCONTROL2_EN_PD);
     printf("  DOB2 enabled: %d\n", FCONTROL2_EN_DOB);
     printf("  Kp=%.3f Kd=%.5f Fkd_FC=%.3f Q_FC=%.3f\n", (float)FCONTROL2_KP, (float)FCONTROL2_KD, (float)FCONTROL2_KD_FC, (float)FCONTROL2_Q_FC);
   }

   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   AnalogIn_Init();
   Quad_init(512, 20000, 500, 500, 500, 16384, 500, 500);
   //Quad_resetCounters();
   //quadGetOffset(); //issue with resetting spring incremental encoder, do it this way
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   setMotorCurrents(0.0, 0.0);
   AnalogOut_Enable(); //enables analog outputs
#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif
   
   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }

   //timer initialization
   if ( (timer_already_running = rt_is_hard_timer_running()) ){
    printf("timer is already running\n");
    period = nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC);
   }
   else {
    printf("timer is not already running, starting timer\n");
    rt_set_periodic_mode();
    period = start_rt_timer(nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC));
   }

  //wait to begin
   printf("waiting %d seconds to begin..go hold the arm\n", TIME_TO_BEGIN);

   while(startCountDown > 0.0 && !End){
     printf("%0.0f seconds to start\n",startCountDown);
     startCountDown -= 1.0; 
     sleep(1);
   }
   printf("starting...\n");

   fflush(stdout);

   //keep program from paging
   mlockall(MCL_CURRENT | MCL_FUTURE); 
   rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;

   while(!End){

     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);
     //printf("sample period %lf\n", samplePeriod_s);
     if(samplePeriod_s > CONTROL_PERIOD_SEC*2) printf(":( %f\t%f\n", samplePeriod_s, elapsedTime_s); //check for jitter

     //printf("elapsedTime:%f\n", elapsedTime_s);
     actuator1.setSampleTime(elapsedTime_s, samplePeriod_s);
     actuator2.setSampleTime(elapsedTime_s, samplePeriod_s);

     //read actuator state and put in local struct
     if(ActuatorSel & ACTUATOR1_MASK) {
       if(actuator1.updateState(Safeness)){
         End = 1;
         break;
       }

       if(elapsedTime_s <= F_SETPOINT_BLEND_SLEWRATE){ //start by slowly increasing force to grav comp force
         desiredForce_n = getDesiredActuatorForce(elapsedTime_s, samplePeriod_s, actuator1.getForce(), 0.0, gravityCompForce(actuator1.getArmAngle()), 0.0, CONST, true, &End);
         actuator1.servoForce(desiredForce_n);
       }
       else{ //then transition into position control
         getDesiredPosition(&desiredPosition_rad, &desiredVelocity_radps, elapsedTime_s-F_SETPOINT_BLEND_SLEWRATE, actuator1.getArmAngle(), deg2rad(Switching_offset_deg), deg2rad(Switching_amp_deg), Switching_freq_hz, 0.0, Mode, true, &End);
         actuator1.servoPosition(desiredPosition_rad,desiredVelocity_radps);
       }
       setMotor1Current(actuator1.getDesiredMotorCurrent());

       //write actuator state to shared mem
       ass_writeShm(rtf_fd, ass, actuator1.getState());
     }

     if(ActuatorSel & ACTUATOR2_MASK) {
       if(actuator2.updateState(Safeness)){
         End = 1;
         break;
       }

       if(elapsedTime_s <= F_SETPOINT_BLEND_SLEWRATE){ //start by slowly increasing force to grav comp force
         desiredForce_n = getDesiredActuatorForce(elapsedTime_s, samplePeriod_s, actuator2.getForce(), 0.0, gravityCompForce(actuator2.getArmAngle()), 0.0, CONST, true, &End);
         actuator2.servoForce(desiredForce_n);
       }
       else{ //then transition into position control
         getDesiredPosition(&desiredPosition_rad, &desiredVelocity_radps, elapsedTime_s-F_SETPOINT_BLEND_SLEWRATE, actuator2.getArmAngle(), deg2rad(Switching_offset_deg), deg2rad(Switching_amp_deg), Switching_freq_hz, 0.0, Mode, true, &End);
         actuator2.servoPosition(desiredPosition_rad,desiredVelocity_radps);
       }
       setMotor2Current(actuator2.getDesiredMotorCurrent());

       //write actuator state to shared mem
       ass_writeShm(rtf_fd2, ass2, actuator2.getState());
     }

#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     //setPortA(HEARTBEAT_CHAN); //check computation load
     rt_task_wait_period();
     //clrPortA(HEARTBEAT_CHAN);
   }

   rt_make_soft_real_time();
   if ( !timer_already_running ){
    printf("stopping timer\n");
    stop_rt_timer();
   }
   rt_task_delete(task);
   rtai_free(nam2num(SHMNAM_ASS), ass);
   rtai_free(nam2num(SHMNAM_ASS2), ass2);
   close(rtf_fd);
   close(rtf_fd2);
   printf("ending...\n");
   setMotorCurrents(0.0, 0.0);
   //AnalogOut_Disable();

   return 0;
}



