#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include <classyFilters.hpp>
#include <digital_out.h>
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include "actuator.hpp"
#include "actuator_utils.h"

static float Rel_spring_pos_mm_offset=sensorAng2springPos_mm(deg2rad(SPRING_ENC_OFFSET_DEG));
static float Rel_bs_pos_m_offset1=motorAng2bsPos_m(deg2rad(MOTOR_ENC_OFFSET_DEG));
static float Rel_bs_pos_m_offset2=motorAng2bsPos_m(deg2rad(MOTOR_ENC_OFFSET_DEG));

//sample load cell force (analog)
//value is in newtons
int sampleLoadCellForce(float *force, char actuatorSel){
  float adc_voltage;
  if(actuatorSel == ACTUATOR1){
    if( sampleADC_5VU(A_LOADCELL_CHAN, &adc_voltage) == ERR_NONE ) {
      //printf("lc_voltage: %f\n", adc_voltage);
      *force = voltage2LoadCellForce_N(adc_voltage);
      return ERR_NONE;
    }
    else {

      printf("Analog_In: Uh oh..sample error\n");
      return ERR;
    }
  }
  else if(actuatorSel == ACTUATOR2){
    if( sampleADC_5VU(A_LOADCELL2_CHAN, &adc_voltage) == ERR_NONE ) {
      //printf("lc_voltage: %f\n", adc_voltage);
      *force = voltage2LoadCellForce_N(adc_voltage);
      return ERR_NONE;
    }
    else {

      printf("Analog_In: Uh oh..sample error\n");
      return ERR;
    }
  }
  return ERR;
}

//sample electrical power (analog)
//value is in watts
int sampleElectricalPower(float *power, char actuatorSel){
  float adc_voltage;
  if(actuatorSel == ACTUATOR1){
    if( sampleADC_5VU(A_EPOWER_CHAN, &adc_voltage) == ERR_NONE ) {
      //printf("lc_voltage for chan %d: %f\n", A_EPOWER_CHAN, adc_voltage);
      *power = voltage2EPower_W(adc_voltage);
      return ERR_NONE;
    }
    else {

      printf("Analog_In: Uh oh..sample error\n");
      return ERR;
    }
  }
  else if(actuatorSel == ACTUATOR2){
    if( sampleADC_5VU(A_EPOWER2_CHAN, &adc_voltage) == ERR_NONE ) {
      //printf("lc_voltage for chan %d: %f\n", A_EPOWER_CHAN, adc_voltage);
      *power = voltage2EPower_W(adc_voltage);
      return ERR_NONE;
    }
    else {

      printf("Analog_In: Uh oh..sample error\n");
      return ERR;
    }
  }
  return ERR;
}

//write local struct to shared memory
void ass_writeShm(unsigned int fd, Actuator_state_str *Ass, Actuator_state_str *localAss){
  rtf_sem_wait(fd);
  memcpy((void*)Ass, (const void*)localAss, sizeof(Actuator_state_str));
  rtf_sem_post(fd);
}

//check if arm is within a safe operating region
//false = we're ok
//true = aaah!
int armPosSafetyCheck(float Arm_ang_rad){
  //printf("ang:%f\tmax:%f\tmin:%f\n",rad2deg(Arm_ang_rad), SOFT_MAX_ARM_ANGLE_DEG, SOFT_MIN_ARM_ANGLE_DEG);
  return (rad2deg(Arm_ang_rad) > SOFT_MAX_ARM_ANGLE_DEG) || (rad2deg(Arm_ang_rad) < SOFT_MIN_ARM_ANGLE_DEG);
}

//check if arm torque is within a safe operating region
//false = we're ok
//true = aaah!
int armTorqueSafetyCheck(float Arm_torque_nm){
  return abs(Arm_torque_nm) >= MAX_TORQUE_NM;
}

//check if ballscrew force is within a safe operating region
//false = we're ok
//true = aaah!
int actuatorForceSafetyCheck(float Actuator_force_n){
  return abs(Actuator_force_n) >= MAX_FORCE_N;
}

void setMotor1Current(float motor_current_des_A){
  outputDAC_10VB(MOTOR1_CHAN, currentOut2dacVoltage(MOTOR_TORQUE_DIRECTION*motor_current_des_A) + MOTOR1_CURRENT_OFFSET_ADJUST_V);
#if EN_MIRROR_OUTPUT
  outputDAC_10VB(MIRROR_CHAN, currentOut2dacVoltage(MOTOR_TORQUE_DIRECTION*motor_current_des_A));
#endif

  /*
  if(fabs(motor_current_des_A) > MAX_CONTINUOUS_MOTOR_CURRENT){
    setPortA(MOTOR_THERMAL_CHAN);
  }
  else{
    clrPortA(MOTOR_THERMAL_CHAN);
  }
  */
}

void setMotor2Current(float motor_current_des_A){
  outputDAC_10VB(MOTOR2_CHAN, currentOut2dacVoltage(MOTOR_TORQUE_DIRECTION*motor_current_des_A) + MOTOR2_CURRENT_OFFSET_ADJUST_V);

  /*
  if(fabs(motor_current_des_A) > MAX_CONTINUOUS_MOTOR_CURRENT){
    setPortA(MOTOR2_THERMAL_CHAN);
  }
  else{
    clrPortA(MOTOR2_THERMAL_CHAN);
  }
  */
}

//scale and output to motor and mirror channel if enabled
void setMotorCurrents(float motor1_current_des_A, float motor2_current_des_A){
  //printf("currenDes:%f\n",current_des_A);
  setMotor1Current(motor1_current_des_A);
  setMotor2Current(motor2_current_des_A);
}

//if you reset the quad counters in hardware, the incremental spring encoder will develop an offset upon movement
//use this function instead and subtract the measured offset
void quadGetOffset(){
  Rel_spring_pos_mm_offset = sensorAng2springPos_mm(Quad_getAngle_rad(Q_SPRING_ANGLE_CHAN));
  Rel_bs_pos_m_offset1 = motorAng2bsPos_m(Quad_getAngle_rad(Q_MOTOR_ANGLE_CHAN));
  Rel_bs_pos_m_offset2 = motorAng2bsPos_m(Quad_getAngle_rad(Q_MOTOR2_ANGLE_CHAN));
}

float getSpringOffset(void){
  return Rel_spring_pos_mm_offset;
}

float getMotor1Offset(void){
  return Rel_bs_pos_m_offset1;
}

float getMotor2Offset(void){
  return Rel_bs_pos_m_offset2;
}

float bound(float in, float lower, float upper){
  float out = in;
  if(out < lower) out = lower;
  else if(out > upper) out = upper;
  return out;
}


