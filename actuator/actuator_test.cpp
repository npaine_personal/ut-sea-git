//impedance control
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include "actuator.hpp"

int main(int argc, char **argv) {

	SEA_Actuator actuator1;

	Rigid_Actuator actuator2;

	actuator1.printType();
	actuator2.printType();

	return 0;
}
