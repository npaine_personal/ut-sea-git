#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <classyFilters.hpp>
#include <utils.h>
#include "actuator.hpp"
#include "actuator_utils.h"
#include "actuator_params.h"
#include <math.h>
#include <analog_in.h>
#include <quadrature.h>
#include <queue>

Actuator::Actuator(void) : Vmot_filt(1), Vact_filt(1), Varm_filt(1), Fkd_filt(1), Pff_filt(2), Qpd_filt(), PDOB_filt()  {
    memset(&State, 0, sizeof(Actuator_state_str));
    ActuatorType = "Base";
    FirstRun_State = true;
    FirstRun_FControl = true;
    FirstRun_ImpControl = true;
    FirstRun_PosControl = true;
}

Actuator::Actuator(int Qpd_len, int PDOB_len) : Vmot_filt(1), Vact_filt(1), Varm_filt(1), Fkd_filt(1), Pff_filt(2), Qpd_filt(Qpd_len), PDOB_filt(PDOB_len)  {
    memset(&State, 0, sizeof(Actuator_state_str));
    ActuatorType = "Base";
    FirstRun_State = true;
    FirstRun_FControl = true;
    FirstRun_ImpControl = true;
    FirstRun_PosControl = true;
}

SEA_Actuator::SEA_Actuator(void) : Actuator(PCONTROL1_QP_FILT_LEN, PCONTROL1_PDOB_FILT_LEN), DOB_filt(FCONTROL1_TDOB_FILT_LEN), Q_filt(FCONTROL1_Q_FILT_LEN) {
    ActuatorType = "SEA";
    SEA_Actuator::sensorInit();
    SEA_Actuator::initFilters(CONTROL_PERIOD_SEC);
}

Rigid_Actuator::Rigid_Actuator(void) : Actuator(PCONTROL2_QP_FILT_LEN, PCONTROL2_PDOB_FILT_LEN), Q_filt(FCONTROL2_Q_FILT_LEN) {
    ActuatorType = "Rigid";
    Rigid_Actuator::sensorInit();
    Rigid_Actuator::initFilters(CONTROL_PERIOD_SEC);
}

Motor::Motor(void) : Actuator(), Q_filt(FCONTROL2_Q_FILT_LEN) {
    ActuatorType = "Motor";
    Motor::sensorInit();
    Motor::initFilters(CONTROL_PERIOD_SEC);
}

void Actuator::initFilters(float samplePeriod_sec){
  Vmot_filt.setSampleTime(samplePeriod_sec);
  Vact_filt.setSampleTime(samplePeriod_sec);
  Varm_filt.setSampleTime(samplePeriod_sec);
  Fkd_filt.setSampleTime(samplePeriod_sec);
  Pff_filt.setSampleTime(samplePeriod_sec);

  Vmot_filt.calculateCoeffs(VEL_MOTOR_FC);
  Vact_filt.calculateCoeffs(VEL_ACTUATOR_FC);
  Varm_filt.calculateCoeffs(VEL_ARM_FC);
}

void SEA_Actuator::initFilters(float samplePeriod_sec){
  Actuator::initFilters(samplePeriod_sec);

  DOB_filt.setSampleTime(samplePeriod_sec);
  Q_filt.setSampleTime(samplePeriod_sec);
  Qpd_filt.setSampleTime(samplePeriod_sec);
  PDOB_filt.setSampleTime(samplePeriod_sec);

  Fkd_filt.calculateCoeffs(FCONTROL1_KD_FC);
  Q_filt.calculateCoeffs(FCONTROL1_Q_FC);
  DOB_filt.calculateCoeffs(FCONTROL1_Q_FC, M_FIT, B_FIT, SPRING_CONST_NpM, FCONTROL1_KP, FCONTROL1_KD, FCONTROL1_KD_FC, SPEED_REDUCTION, DRIVETRAIN_EFF, TORQUE_CONSTANT);
  Pff_filt.calculateCoeffs(PCONTROL1_FC, ARM_INERTIA, ARM_FRICTION );
  Qpd_filt.calculateCoeffs(PCONTROL1_DOB_FC);
  PDOB_filt.calculateCoeffs(PCONTROL1_FC, PCONTROL1_DOB_FC, M_FIT, B_FIT, SPRING_CONST_NpM, FCONTROL1_KP, FCONTROL1_KD, FCONTROL1_KD_FC, SPEED_REDUCTION, DRIVETRAIN_EFF, TORQUE_CONSTANT);
}

void Rigid_Actuator::initFilters(float samplePeriod_sec){
  Actuator::initFilters(samplePeriod_sec);

  Q_filt.setSampleTime(samplePeriod_sec);

  Fkd_filt.calculateCoeffs(FCONTROL2_KD_FC);
  Q_filt.calculateCoeffs(FCONTROL2_Q_FC);
  Pff_filt.calculateCoeffs(PCONTROL2_FC, ARM_INERTIA, ARM_FRICTION );
}

void Motor::initFilters(float samplePeriod_sec){
  Actuator::initFilters(samplePeriod_sec);

  Q_filt.setSampleTime(samplePeriod_sec);

  Fkd_filt.calculateCoeffs(FCONTROL2_KD_FC);
  Q_filt.calculateCoeffs(FCONTROL2_Q_FC);
}

void Actuator::printType(void){
  std::cout << ActuatorType << std::endl;
}

void Actuator::fillPosQueue(float value){
  int i;

  for(i=0; i<posQueueSize; i++){
    posQueue.push(value);
  }
}

void Actuator::fillVelQueue(float value){
  int i;

  for(i=0; i<velQueueSize; i++){
    velQueue.push(value);
  }
}

int Actuator::updateState(int checkSafety) {

  grabStateData();

  if(FirstRun_State){
    Vmot_filt.init(State.Motor_angle_rad);
    Vact_filt.init(State.Actuator_pos_m);
    Varm_filt.init(State.Arm_ang_rad);
    //Varm_filt.printState();
  }

  State.Arm_torque_nm = actuatorForce2ArmTorque(State.Actuator_force_n, State.Arm_ang_rad);
  State.Motor_vel_radps = Vmot_filt.next(State.Motor_angle_rad); //rad/sec
  State.Actuator_vel_mps = Vact_filt.next(State.Actuator_pos_m);
  State.Motor_mech_power_w = motorCurrent2Torque(State.Motor_current_des_amps) * State.Motor_vel_radps;

  State.Arm_vel_radps = Varm_filt.next(State.Arm_ang_rad); //rad/sec
  State.Arm_power_w = State.Arm_torque_nm * State.Arm_vel_radps;
  State.Mech_efficiency = -State.Arm_power_w / State.Motor_mech_power_w;
  if(State.Mech_efficiency > 1.0 || State.Mech_efficiency < 0.0) State.Mech_efficiency = -2.0; //check validity of data
  //State.Motor_voltage = calcMotorVoltage(State.Motor_current_des_amps, radps2rpm(State.Motor_vel_radps));
  //State.Motor_elec_power_w = fabs(State.Motor_voltage * State.Motor_current_des_amps);
  State.Motor_efficiency = State.Motor_mech_power_w / State.Motor_elec_power_w;
  if(State.Motor_efficiency > 1.0 || State.Motor_efficiency < 0.0) State.Motor_efficiency = -1.0; //check validity of data
  State.Total_efficiency = State.Motor_efficiency * State.Mech_efficiency;

  State.newData = 1;
  FirstRun_State = false;
  
  //safety checks
  if(checkSafety){
    if(actuatorForceSafetyCheck(State.Actuator_force_n)){ 
     printf("Actuator force (%.2fN) greater than safe value (%.2fN)\n", State.Actuator_force_n, (float)MAX_FORCE_N);
     return 1;
    }
    else if(armTorqueSafetyCheck(State.Arm_torque_nm)){ 
      printf("Arm torque (%.2fNm) greater than safe value (%.2fNm)\n", State.Arm_torque_nm, (float)MAX_TORQUE_NM);
      return 1;
    }
    else if(armPosSafetyCheck(State.Arm_ang_rad)){ 
      printf("Arm position (%.2fdeg) out of safe region (%.2f->%.2fdeg)\n", rad2deg(State.Arm_ang_rad), (float)SOFT_MAX_ARM_ANGLE_DEG, (float)SOFT_MIN_ARM_ANGLE_DEG);
      return 1;
    }
    else { return 0; }
  }
  else return 0;

}

void SEA_Actuator::grabStateData(void) {
  float rel_spring_pos_mm, rel_bs_pos_m;
  float bs_pos_m;
  float spring_pos_mm;

  //analog, could be slow
  sampleLoadCellForce(&(State.LoadCell_force_n), ACTUATOR1);
  sampleElectricalPower(&(State.Motor_elec_power_w), ACTUATOR1);

  //digital, should be fast
  rel_spring_pos_mm = sensorAng2springPos_mm(Quad_getAngle_rad(Q_SPRING_ANGLE_CHAN)) - getSpringOffset();
  State.Motor_angle_rad = Quad_getAngle_rad(Q_MOTOR_ANGLE_CHAN);
  spring_pos_mm = Init_spring_pos_mm - rel_spring_pos_mm;

  rel_bs_pos_m = motorAng2bsPos_m(State.Motor_angle_rad) - getMotor1Offset();
  bs_pos_m = Init_bs_pos_m + rel_bs_pos_m;
  State.Actuator_pos_m = bs_pos_m - spring_pos_mm/1000.0;
  State.Actuator_force_n = springPos_mm2springForce_N(spring_pos_mm);

  State.Arm_ang_rad = actuator1Len2ArmAngle(State.Actuator_pos_m);
}

void Rigid_Actuator::grabStateData(void) {
  float rel_bs_pos_m;
  float bs_pos_m;

  //analog, could be slow
  sampleLoadCellForce(&(State.LoadCell_force_n), ACTUATOR2);
  sampleElectricalPower(&(State.Motor_elec_power_w), ACTUATOR2);

  //digital, should be fast
  State.Motor_angle_rad = Quad_getAngle_rad(Q_MOTOR2_ANGLE_CHAN);

  rel_bs_pos_m = motorAng2bsPos_m(State.Motor_angle_rad) - getMotor2Offset();
  bs_pos_m = Init_bs_pos_m + rel_bs_pos_m;
  State.Actuator_pos_m = bs_pos_m;
  State.Actuator_force_n = State.LoadCell_force_n;

  State.Arm_ang_rad = actuator2Len2ArmAngle(State.Actuator_pos_m);
}

void Motor::grabStateData(void) {

  //analog, could be slow
  //sampleLoadCellForce(&(State.LoadCell_force_n), ACTUATOR2);
  sampleElectricalPower(&(State.Motor_elec_power_w), ACTUATOR2);

  //digital, should be fast
  State.Motor_angle_rad = Quad_getAngle_rad(Q_MOTOR2_ANGLE_CHAN);

  //rel_bs_pos_m = motorAng2bsPos_m(State.Motor_angle_rad) - getMotor2Offset();
  //bs_pos_m = Init_bs_pos_m + rel_bs_pos_m;
  State.Actuator_pos_m = State.Motor_angle_rad;  //hack
  //State.Actuator_force_n = State.LoadCell_force_n;

}

//finds initial (absolute) arm angle and initial (absolute) spring deflection
//uses these values to calculate initial actuator length
void Motor::sensorInit(void){
  //start by averaging samples of absolute analog sensors
  //err = getAvgSensorValues(&Init_spring_pos_mm, &Init_arm_ang_rad);

  Init_arm_ang_rad = 0.0;
   
  //next figure out the actuator length
  Init_bs_pos_m = 0.0; 
}

//finds initial (absolute) arm angle and initial (absolute) spring deflection
//uses these values to calculate initial actuator length
void Rigid_Actuator::sensorInit(void){
  //start by averaging samples of absolute analog sensors
  //err = getAvgSensorValues(&Init_spring_pos_mm, &Init_arm_ang_rad);

  Init_arm_ang_rad = deg2rad(ARM_STARTING_POS_DEG);
   
  //next figure out the actuator length
  Init_bs_pos_m = armAngle_rad2Actuator2Length_m(Init_arm_ang_rad); 
}

//finds initial (absolute) arm angle and initial (absolute) spring deflection
//uses these values to calculate initial actuator length
void SEA_Actuator::sensorInit(void){
  //start by averaging samples of absolute analog sensors
  //err = getAvgSensorValues(&Init_spring_pos_mm, &Init_arm_ang_rad);

  Init_arm_ang_rad = deg2rad(ARM_STARTING_POS_DEG);
  Init_spring_pos_mm = 0.0; //assumption
   
  //next figure out the actuator length
  Init_bs_pos_m = armAngle_rad2Actuator1Length_m(Init_arm_ang_rad) + Init_spring_pos_mm/1000.0; 
}

void Actuator::setMotorCurrent(float current){ 
  State.Motor_current_des_amps = bound(current, -ELMO_PEAK_CURRENT*ELMO_PEAK_SAFE_PERCENTAGE, ELMO_PEAK_CURRENT*ELMO_PEAK_SAFE_PERCENTAGE);
}

void SEA_Actuator::servoForce(float force_des){
  float motor_current_des = 0;
  float force_ref = force_des;
  float force_meas = State.Actuator_force_n;
#if FCONTROL1_EN_PD
  float force_err;
#endif
#if FCONTROL1_EN_DOB
  float force_des_d;
#endif

  State.Actuator_force_des_n = force_des; //save desired value into state

  if(FirstRun_FControl){
    DOB_filt.init(force_meas);
    Q_filt.init(force_ref);
    Force_ref_prev = force_ref;
  }


#if FCONTROL1_EN_DOB
  //disturbance observer filter (DOB)
  force_des_d = DOB_filt.next(force_meas) - Q_filt.next(Force_ref_prev);

  force_ref -= force_des_d;
  Force_ref_prev = force_ref;
#endif

  motor_current_des += BsForce2motorCurrent(force_ref); //feedforward control effort

#if FCONTROL1_EN_PD
  //feedback compensation filter (PD feedback)
  force_err = force_ref - force_meas;

  if(FirstRun_FControl){
    Fkd_filt.init(force_err);
  }
  motor_current_des += FCONTROL1_KP*force_err + FCONTROL1_KD*Fkd_filt.next(force_err);
#endif

  setMotorCurrent(motor_current_des);
  
  FirstRun_FControl = false;
}


void Rigid_Actuator::servoForce(float force_des){
  float motor_current_des = 0;
  float force_ref = force_des;
#if FCONTROL2_EN_PD
  float force_meas = State.Actuator_force_n;
  float force_err;
#endif
#if FCONTROL2_EN_DOB
  float force_des_d;
#endif

  State.Actuator_force_des_n = force_des; //save desired value into state

  if(FirstRun_FControl){
    Q_filt.init(force_ref);
    Force_ref_prev = force_ref;
  }

#if FCONTROL2_EN_DOB
  //disturbance observer filter (DOB)
  force_des_d = Q_filt.next(force_meas-Force_ref_prev);

  force_ref -= force_des_d;
  Force_ref_prev = force_ref;
#endif

  motor_current_des += BsForce2motorCurrent(force_ref); //feedforward control effort

#if FCONTROL2_EN_PD
  //feedback compensation filter (PD feedback) 
  force_err = force_ref - force_meas;

  if(FirstRun_FControl){
    Fkd_filt.init(force_err);
  }

  motor_current_des += FCONTROL2_KP*force_err + FCONTROL2_KD*Fkd_filt.next(force_err);
#endif

  setMotorCurrent(motor_current_des);
  
  FirstRun_FControl = false;
}

void Motor::servoForce(float force_des){

  setMotorCurrent(motorTorque2current(force_des));
  
  FirstRun_FControl = false;
}

void Actuator::servoImpedance(float posDes, float velDes, float effortDes, float stiffness, float damping){
  float stiffnessEffort, dampingEffort, error, error_dot, totalDesiredEffort;
  float posMeas = State.Actuator_pos_m;
  float velMeas = State.Actuator_vel_mps;

  State.Actuator_pos_des_m = posDes; //save desired value into state
  State.Actuator_vel_des_mps = velDes; //save desired value into state

  if(FirstRun_ImpControl){
    fillPosQueue(State.Actuator_pos_m);
    fillVelQueue(State.Actuator_vel_mps);
  }

  //artificial latency
  posQueue.push(State.Actuator_pos_m);
  velQueue.push(State.Actuator_vel_mps);
  posMeas = posQueue.front();
  velMeas = velQueue.front();
  posQueue.pop();
  velQueue.pop();

  //do control stuff
  error = posDes - posMeas;
  error_dot = velDes - velMeas;
  stiffnessEffort = stiffness * error;
  dampingEffort = damping * error_dot;

  totalDesiredEffort = effortDes + stiffnessEffort + dampingEffort;

  servoForce(totalDesiredEffort);

  FirstRun_ImpControl = false;
}

//servo output position using a disturbance observer and a model of the mass-damper load
void Actuator::servoPosition(float posDes, float velDes){
  float ang_des_rel, ang_meas_rel, torque_des_nm;
#if PCONTROL1_EN_DOB
  float ang_des_d;
#endif

  State.Arm_ang_des_rad = posDes; //save desired value into state
  State.Arm_vel_des_radps = velDes; //save desired value into state

  if(FirstRun_PosControl){
    PosCntrl_arm_ang_offset_rad = State.Arm_ang_rad;
  }
    
  //we have to servo on relative angle
  ang_des_rel = posDes - PosCntrl_arm_ang_offset_rad;  
  ang_meas_rel = State.Arm_ang_rad - PosCntrl_arm_ang_offset_rad;

  if(FirstRun_PosControl){
    PosCntrl_arm_ang_des_prev = ang_des_rel;
    Qpd_filt.init(ang_des_rel);
    PDOB_filt.init(ang_meas_rel);
    Pff_filt.init(ang_des_rel);
  }

#if PCONTROL1_EN_DOB
  //pdob
  ang_des_d = PDOB_filt.next(ang_meas_rel) - Qpd_filt.next(PosCntrl_arm_ang_des_prev); 
  ang_des_rel -= ang_des_d; //force system to behave like model
  PosCntrl_arm_ang_des_prev = ang_des_rel;
#endif

  //feedforward compensation
  torque_des_nm = Pff_filt.next(ang_des_rel);

  //gravity compensation torque
  torque_des_nm += armGravityTorque(State.Arm_ang_rad); //changed from -=

  //transform to desired force and send to force controller
  servoForce( armTorque_nm2BsForce_N(torque_des_nm, State.Arm_ang_rad) );

  FirstRun_PosControl = false;
}








