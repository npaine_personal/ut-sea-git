//logs actuator data to file
//run during the 10 second wait before test programs starts
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <utils.h>
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include "actuator_params.h"
#include "actuator.hpp"


#define RTAI_TASK_NAME "DLOGR" //must be unique

#define SAMPLE_FREQ_HZ 1000
#define SAMPLE_PERIOD_SEC (1.0/(float)SAMPLE_FREQ_HZ)
#define SAMPLE_DURATION_S 0 
#define LOGFILE1_DEFAULT "actuator1_data.csv" //tab-separated values
#define LOGFILE2_DEFAULT "actuator2_data.csv" //tab-separated values
#define FILE_DELIMITER ","
#define CONSOLE_DELIMITER "\t"

static char *LogFile1, *LogFile2;

static int SampleFreq_Hz = SAMPLE_FREQ_HZ;
static int LogDuration = SAMPLE_DURATION_S;
static char File1_default[] = LOGFILE1_DEFAULT;
static char File2_default[] = LOGFILE2_DEFAULT;
static char ActuatorSel = ACTUATOR1_MASK;

static int End=0;
static void endme(int dummy) { End=1; }

//shared mem struct
static Actuator_state_str *Ass;
static Actuator_state_str *Ass2;

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -r <log rate (hz)>      : sample freq (default :%d)\n", SampleFreq_Hz);
  printf ("  -d <log duration (s)>   : sample duration, 0 for infinite (default: %d)\n", LogDuration);
  printf ("  -t <actuator>           : select actuator to log, 1=actuator1, 2=actuator2, 3=both (default: %d)\n", ActuatorSel);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "r:t:d:wsh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'r':
        SampleFreq_Hz = atoi(optarg);
        break;
      case 'd':
        LogDuration = atoi(optarg);
        break;
      case 't':
        ActuatorSel = atoi(optarg);
        break;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

//try to update local data from shared memory
//1 if we suceed, 0 if theres no new data yet
unsigned char ass_tryReadShm(unsigned int fd, Actuator_state_str *Ass, Actuator_state_str *localAss){
  unsigned char newData = 0;
  rtf_sem_wait(fd);
  if(Ass->newData){ //update local struct if there is new data
    //printf("new data!\n");
    Ass->newData = 0;
    memcpy((void*)localAss, (const void*)Ass, sizeof(Actuator_state_str));
    newData = 1;
  }
  rtf_sem_post(fd);
  return newData;
}

void logHeader(FILE* ifp){
  fprintf(ifp, "Time (sec)" FILE_DELIMITER 
    "Sample Period (sec)" FILE_DELIMITER  
    "Desired Arm Angle(deg)" FILE_DELIMITER  
    "Arm Angle(deg)" FILE_DELIMITER 
    "Arm Velocity(rad/sec)" FILE_DELIMITER 
    "Desired Actuator Pos(m)" FILE_DELIMITER  
    "Actuator Pos(m)" FILE_DELIMITER  
    "Desired Actuator Vel(mps)" FILE_DELIMITER  
    "Actuator Vel(mps)" FILE_DELIMITER 
    "Desired Force(N)" FILE_DELIMITER 
    "Actuator Force(N)" FILE_DELIMITER 
    "LoadCell Force(N)" FILE_DELIMITER 
    "Arm Torque (Nm)" FILE_DELIMITER 
    "Desired Motor Current(A)" FILE_DELIMITER 
    "Motor Angle(rad)" FILE_DELIMITER 
    "Motor Velocity(rpm)" FILE_DELIMITER 
    "Electrical Power(W)" FILE_DELIMITER 
    "Motor Power(W)" FILE_DELIMITER 
    "Arm Power(W)\n");
}

void logData(FILE* ifp, Actuator_state_str* ass){
  fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f"  FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", 
          ass->SampleTime_s, 
          ass->SamplePeriod_s, 
          rad2deg(ass->Arm_ang_des_rad), 
          rad2deg(ass->Arm_ang_rad), 
          ass->Arm_vel_radps, 
          ass->Actuator_pos_des_m, 
          ass->Actuator_pos_m, 
          ass->Actuator_vel_des_mps, 
          ass->Actuator_vel_mps, 
          ass->Actuator_force_des_n, 
          ass->Actuator_force_n, 
          ass->LoadCell_force_n, 
          ass->Arm_torque_nm, 
          ass->Motor_current_des_amps, 
          ass->Motor_angle_rad,
          radps2rpm(ass->Motor_vel_radps), 
          ass->Motor_elec_power_w, 
          ass->Motor_mech_power_w, 
          ass->Arm_power_w);
}

int main(int argc, char **argv) {

   int timer_already_running;

   Actuator_state_str localAss; //contains actuator state data
   unsigned int rtf_fd;

   Actuator_state_str localAss2; //contains actuator state data
   unsigned int rtf_fd2;

   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;

   int iteration = 0, maxIteration = -1;

   //printf("hello\n");

   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv);

   Ass = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore

   Ass2 = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS2), sizeof(Actuator_state_str));
   rtf_fd2 = open(ASS_FIFO2, O_RDWR); //shm access semaphore
   
   //file stuff
   LogFile1 = File1_default;
   LogFile2 = File2_default;
   FILE* ifp1, *ifp2;

   if(ActuatorSel & ACTUATOR1_MASK){
     ifp1 = fopen(LogFile1, "w");
     if (!ifp1) {
       fprintf(stderr, "Cannot open file %s\n", LogFile1);
       exit(1);
     }
   }
   if(ActuatorSel & ACTUATOR2_MASK){
     ifp2 = fopen(LogFile2, "w");
     if (!ifp2) {
       fprintf(stderr, "Cannot open file %s\n", LogFile2);
       exit(1);
     }
   }

   if(LogDuration > 0) {
    maxIteration = LogDuration*SampleFreq_Hz;
   }

   //print header
   printf("Data logging to "); 
   if(ActuatorSel & ACTUATOR1_MASK){
    printf("%s ", LogFile1); 
    logHeader(ifp1);
   }
   if(ActuatorSel & ACTUATOR2_MASK){
    printf("%s ", LogFile2); 
    logHeader(ifp2);
   }
   printf("at %d hz\n", SampleFreq_Hz);

   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }

   //timer initialization
   if ( (timer_already_running = rt_is_hard_timer_running()) ){
    printf("timer is already running\n");
    period = nano2count(NS_PER_SEC/SampleFreq_Hz);
   }
   else {
    printf("timer is not already running, starting timer\n");
    rt_set_periodic_mode();
    period = start_rt_timer(nano2count(NS_PER_SEC/SampleFreq_Hz));
   }
   
   printf("trying to make periodic\n");
   //keep program from paging
   //mlockall(MCL_CURRENT | MCL_FUTURE); 
   //rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;

   printf("starting loop\n");
   fflush(stdout);

   while(!End && iteration != maxIteration){
     //printf("in loop\n");

     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);

     //update our local state struct and print to file if there is new data
     //printf("trying to read shm at t=%f\n", elapsedTime_s);
     
     if(ActuatorSel & ACTUATOR1_MASK) {
       //printf("trying read shm (1)\n");
       if(ass_tryReadShm(rtf_fd, Ass, &localAss) ){
         //printf("read shm (1) at t=%f\n", elapsedTime_s);
         logData(ifp1, &localAss);
       }
     }
     
     if(ActuatorSel & ACTUATOR2_MASK) {
       //printf("trying read shm (2)\n");
       if(ass_tryReadShm(rtf_fd2, Ass2, &localAss2) ){
         //printf("read shm (2) at t=%f\n", elapsedTime_s);
         logData(ifp2, &localAss2);
       }
     }

     //printf("trying to wait\n");
     iteration++;
     rt_task_wait_period();
   }

   //rt_make_soft_real_time();
   if ( !timer_already_running ){
    printf("stopping timer\n");
    stop_rt_timer();
   }
   rt_task_delete(task);
   rtai_free(nam2num(SHMNAM_ASS), Ass);
   rtai_free(nam2num(SHMNAM_ASS2), Ass2);
   close(rtf_fd);
   printf("ending...\ndata written to: ");
   if(ActuatorSel & ACTUATOR1_MASK) { 
    printf("%s\n", LogFile1);
    fclose(ifp1); }
   if(ActuatorSel & ACTUATOR2_MASK) { 
    printf("%s\n", LogFile2);
    fclose(ifp2); }
   

   return 0;
}



