#ifndef _ACTUATOR_UTILS_H
#define _ACTUATOR_UTILS_H

#define EN_MIRROR_OUTPUT 1

#define VM_FC 20
#define VA_FC 20

//finds initial (absolute) arm angle and initial (absolute) spring deflection
//uses these values to calculate initial actuator length
//return 0 if no error, 1 if sample error
int sensorInit(void);


//write local struct to shared memory
void ass_writeShm(unsigned int fd, Actuator_state_str *Ass, Actuator_state_str *localAss);

//check if arm is within a safe operating region
//false = we're ok
//true = aaah!
int armPosSafetyCheck(float Arm_ang_rad);

//check if arm torque is within a safe operating region
//false = we're ok
//true = aaah!
int armTorqueSafetyCheck(float Arm_torque_nm);

//check if ballscrew force is within a safe operating region
//false = we're ok
//true = aaah!
int actuatorForceSafetyCheck(float Arm_force_n);

void initializeFilters(float samplePeriod_sec);

//updates the state of the actuator
//requires time since it was last run to calculate velocities (not currently using this, must be called at 1khz)
//safe = 1, check to make sure values are within safe operating levels
//return 0 if safe (or if safe=0), 1 if not
int updateState(Actuator_state_str *localAss, char actuatorSel, char safe);

void setMotor1Current(float motor_current_des_A);

void setMotor2Current(float motor_current_des_A);

//scale and output to motor and mirror channel if enabled
void setMotorCurrents(float motor1_current_des_A, float motor2_current_des_A);

//if you reset the quad counters in hardware, the incremental spring encoder will develop an offset upon movement
//use this function instead and subtract the measured offset
void quadGetOffset(void);

//sample load cell force (analog)
//value is in newtons
int sampleLoadCellForce(float *force, char actuatorSel);

//sample electrical power (analog)
//value is in watts
int sampleElectricalPower(float *power, char actuatorSel);

float getSpringOffset(void);

float getMotor1Offset(void);

float getMotor2Offset(void);

float bound(float in, float lower, float upper);

#endif //_ACTUATOR_UTILS_H





