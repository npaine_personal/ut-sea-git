#ifndef _ACTUATOR_PARAMS_H
#define _ACTUATOR_PARAMS_H

//frequently tuned params
#define MOTOR_ENC_OFFSET_DEG 0.0
#define SPRING_ENC_OFFSET_DEG 0.2 //(reduce for more grav comp)

#define ARM_STARTING_POS_DEG 53.0
//#define SPRING_SENSOR_ZERO_OFFSET 2.513  //adjust this if there is force/torque offset, increase this value if there is an upwards torque for gravity compensation, decrease if there is a downward torque
//#define ARM_V_PI_RAD 1.000 //sensor voltage for an arm angle of pi radians (change if abs. angle measurement is off)

#define ADDED_MASS_LBS 0.0 //0.0 10.25
#define ADDED_MASS lbs2kg(ADDED_MASS_LBS) //kg
#define ADDED_MASS_DIST .211 //m from arm pivot along arm axis
  // for 0.0                pdobfc = 35 maxforce = 500  maxtorque = 8  chirp_a=15 chirp_f=3 spline_a=30 spline_f=2
  // for 2.475  dist = .190 pdobfc = 13 maxforce = 800  maxtorque = 17 chirp_a=15 chirp_f=2 spline_a=30 spline_f=1.5
  // for 5.314  dist = .190 pdobfc = 13 maxforce = 1200 maxtorque = 30 a=?  f=? spline_a=30 spline_f=1.25
  // for 9.793  dist = .190 pdobfc = 9  maxforce = 1200 maxtorque = 35 a=?  f=? spline_a=30 spline_f=1.0
  // for 19.586 dist = .197 pdobfc = 5  maxforce = 1500 maxtorque = 45 a=10 f=1.2

#define ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT 0

//position limits
#define HARD_MAX_ARM_ANGLE_DEG 168.0
#define HARD_MIN_ARM_ANGLE_DEG 53.0
#define MID_ARM_ANGLE_DEG ((HARD_MAX_ARM_ANGLE_DEG + HARD_MIN_ARM_ANGLE_DEG)/2)
#define ARM_ANGLE_SAFETY_ZONE_DEG 5 //-10 or 5?
#define SOFT_MAX_ARM_ANGLE_DEG (HARD_MAX_ARM_ANGLE_DEG-ARM_ANGLE_SAFETY_ZONE_DEG)
#define SOFT_MIN_ARM_ANGLE_DEG (HARD_MIN_ARM_ANGLE_DEG+ARM_ANGLE_SAFETY_ZONE_DEG)
#define MID_ACTUATOR1_POS_M (armAngle_rad2Actuator1Length_m(deg2rad(MID_ARM_ANGLE_DEG)))
#define MID_ACTUATOR2_POS_M (armAngle_rad2Actuator2Length_m(deg2rad(MID_ARM_ANGLE_DEG)))

//force limits
#define MAX_TORQUE_NM 70  //max: 70
#define MAX_FORCE_N 2750  //max: 2750

//analog input channels
#define A_CURRENT1_CHAN 0
#define A_CURRENT2_CHAN 1
#define A_LOADCELL_CHAN 2
#define A_LOADCELL2_CHAN 3
#define A_EPOWER_CHAN 4
#define A_EPOWER2_CHAN 5

//quadrature input channels
#define Q_MOTOR_ANGLE_CHAN 4 
#define Q_SPRING_ANGLE_CHAN 5 
#define Q_MOTOR2_ANGLE_CHAN 6 

//analog output channels
#define MOTOR1_CHAN 1
#define MOTOR2_CHAN 2
#define MIRROR_CHAN 3

//digital output channels
#define HEARTBEAT_CHAN 1 //port A
#define MOTOR_THERMAL_CHAN 2 //port A

//spring displacement params
#define SPRING_PULLEY1_DIAM_MM 7.239 //absolute sensor
#define SPRING_THROW_MM (PI*SPRING_PULLEY1_DIAM_MM)
#define SUPPLY_V_SPRING 3.9337 //effective supply voltage (tuning param)
#define SPRING_PULLEY2_DIAM_MM 7.239 //incremental sensor
#define SPRING_CONST_NpMM 350 //was 277.78
#define SPRING_CONST_NpM (SPRING_CONST_NpMM*1000)
#define SPRING_ENC_DIRECTION 1

//arm position params
//#define SUPPLY_V_ARM 4.85 //effective supply voltage (tuning param, affects abs to inc angle scale)
//#define ARM_CALIB_ANG PI

//load cell params
#define LC_VOFFSET 2.55 //circuit voltage offset
#define LC_GAIN 198.0 //circuit gain
#define LC_RATED_FORCE_N 2224.11 //newtons
#define LC_SENSITIVITY 0.0020656 //from calibration sheet
#define LC_VEXCITE 5.0 //excitation voltage

//electrical power measurement params
#define EPOWER_VOFFSET 0.0//0.006
#define V_BUS 80.0
#define EPOWER_GAIN 16.34 //16.82
#define R_SENSE_OHMS 0.01

//drivetrain params
#define BIG_PULLEY_TEETH 60.0
#define SMALL_PULLEY_TEETH 15.0
#define PULLEY_REDUCTION (BIG_PULLEY_TEETH/SMALL_PULLEY_TEETH)
#define BALLSCREW_LEAD .003 //m
#define DRIVETRAIN_EFF 0.9 //efficiency
#define SPEED_REDUCTION (PULLEY_REDUCTION*2*PI/BALLSCREW_LEAD)

//other params
//#define INIT_NUM_SAMPLES 1000 //used for averaging initial analog measurement

//mechanical params
#define A1_C_LENGTH_M 0.15436
#define A2_C_LENGTH_M 0.1225
#define B_LENGTH_M 0.025
#define SPRUNG_MASS .706 //kg
#define ROTOR_INERTIA 0.00000344900 //kgm^2
#define BALL_NUT_INERTIA 0.000017523 //kgm^2
#define EFFECTIVE_ROTOR_INERTIA (ROTOR_INERTIA + BALL_NUT_INERTIA/PULLEY_REDUCTION/PULLEY_REDUCTION)
#define G 9.80665
#define ARM_MASS 0.665 //kg
#define ARM_X_COM_DIST 0.157
#define ARM_Y_COM_DIST -0.001 
#define ARM_COM_ANG_OFFSET (atan2(ARM_Y_COM_DIST,ARM_X_COM_DIST)) //rad
#define ARM_GEOM_VERT_OFFSET 0.0 //m, distace arm axis is from rotation point
#define COMBINED_ARM_COM_X (ARM_X_COM_DIST*ARM_MASS + ADDED_MASS_DIST*ADDED_MASS)/(ARM_MASS+ADDED_MASS)
#define COMBINED_ARM_COM_Y (ARM_Y_COM_DIST*ARM_MASS + ARM_GEOM_VERT_OFFSET*ADDED_MASS)/(ARM_MASS+ADDED_MASS)
#define COMBINED_ARM_COM_ANG_OFFSET (atan2(COMBINED_ARM_COM_Y,COMBINED_ARM_COM_X))
#define COMBINED_ARM_COM_DIST (sqrt(COMBINED_ARM_COM_Y*COMBINED_ARM_COM_Y + COMBINED_ARM_COM_X*COMBINED_ARM_COM_X))
#define COMBINED_ARM_COM_MASS (ARM_MASS+ADDED_MASS)
#define MOUNT_ANG_OFFSET 0.0389 //rad, angle from actuator mount pivot to joint pivot from horizontal

//force control
#define M_FIT 360 //kg
#define B_FIT 2200 //ns/m

//position control
#define ARM_INERTIA (COMBINED_ARM_COM_MASS*COMBINED_ARM_COM_DIST*COMBINED_ARM_COM_DIST) //test this first
#define ARM_FRICTION 0.065 //Nm*s/rad (viscous)

//thermal params
#define MAX_CONTINUOUS_MOTOR_CURRENT 4.7 //A
#define MAX_CONTINUOUS_MOTOR_TORQUE motorCurrent2Torque(MAX_CONTINUOUS_MOTOR_CURRENT) //Nm

//motor and electrical params
#define DAC_VOLTAGE 10.0
#define ELMO_PEAK_CURRENT 30.0
#define ELMO_PEAK_SAFE_PERCENTAGE 0.99 //may be a/d issue, but motor misbehaves if capped at 30 amps exactly
#define MOTOR1_CURRENT_OFFSET_ADJUST_V -0.03 //volts at DAC output
#define MOTOR2_CURRENT_OFFSET_ADJUST_V -0.045 //volts at DAC output
#define MOTOR_TORQUE_DIRECTION -1
#define TORQUE_CONSTANT .0276 //nm/A
#define SPEED_CONSTANT 345.99 //rpm/v
#define WINDING_RESISTANCE 0.386 //ohms

//conversions
#define rot2rad(rot) ((float)(rot)*2*PI)
#define rad2rot(rad) ((float)(rad)/rot2rad(1))
#define voltage2springPos_mm(voltage) ((-((voltage)-SPRING_SENSOR_ZERO_OFFSET)*SPRING_THROW_MM)/SUPPLY_V_SPRING)
#define voltage2armAng_rad(voltage) (((ARM_V_PI_RAD-(float)(voltage))*2*PI)/SUPPLY_V_ARM + ARM_CALIB_ANG)
#define motorAng2bsPos_m(mang) ((float)(mang)*BALLSCREW_LEAD/2/PI/PULLEY_REDUCTION)
#define sensorAng2springPos_mm(sang) (SPRING_ENC_DIRECTION*(float)(sang)*SPRING_PULLEY2_DIAM_MM/2)
#define springPos_mm2springForce_N(pos) ((float)(pos)*SPRING_CONST_NpMM)
#define springForce_N2springPos_mm(force) ((float)(force)/SPRING_CONST_NpMM)

#define armAngle_rad2Actuator1Length_m(angle_rad) sqrt( B_LENGTH_M*B_LENGTH_M + A1_C_LENGTH_M*A1_C_LENGTH_M - 2*B_LENGTH_M*A1_C_LENGTH_M*cos((float)(angle_rad)) ) //finds actuator length given arm angle using law of cosines
#define armAngle_rad2Actuator2Length_m(angle_rad) sqrt( B_LENGTH_M*B_LENGTH_M + A2_C_LENGTH_M*A2_C_LENGTH_M - 2*B_LENGTH_M*A2_C_LENGTH_M*cos((float)(angle_rad)) ) //finds actuator length given arm angle using law of cosines
#define actuator1Len2ArmAngle(len) (acos((-((float)(len)*(float)(len)) + B_LENGTH_M*B_LENGTH_M + A1_C_LENGTH_M*A1_C_LENGTH_M)/(2*B_LENGTH_M*A1_C_LENGTH_M) ))
#define actuator2Len2ArmAngle(len) (acos((-((float)(len)*(float)(len)) + B_LENGTH_M*B_LENGTH_M + A2_C_LENGTH_M*A2_C_LENGTH_M)/(2*B_LENGTH_M*A2_C_LENGTH_M) ))
#define currentOut2dacVoltage(current) ((float)(current)*DAC_VOLTAGE/ELMO_PEAK_CURRENT)
#define motorTorque2current(torque)  ((float)(torque)/TORQUE_CONSTANT)
#define calcMotorVoltage(current, rpm) ((float)(current)*WINDING_RESISTANCE + (float)(rpm)/SPEED_CONSTANT)
#define BsForce2motorTorque(force) ((float)(force)*BALLSCREW_LEAD/(PULLEY_REDUCTION*2*PI*DRIVETRAIN_EFF))
#define motorTorque2BsForce(torque) ((float)(torque)*PULLEY_REDUCTION*2*PI*DRIVETRAIN_EFF/BALLSCREW_LEAD)
#define linkage1LeverArm(angle_rad) ( (B_LENGTH_M*A1_C_LENGTH_M*sin((float)(angle_rad)))/sqrt( B_LENGTH_M*B_LENGTH_M + A1_C_LENGTH_M*A1_C_LENGTH_M - 2*B_LENGTH_M*A1_C_LENGTH_M*cos((float)(angle_rad))) )
#define linkage2LeverArm(angle_rad) ( (B_LENGTH_M*A2_C_LENGTH_M*sin((float)(angle_rad)))/sqrt( B_LENGTH_M*B_LENGTH_M + A2_C_LENGTH_M*A2_C_LENGTH_M - 2*B_LENGTH_M*A2_C_LENGTH_M*cos((float)(angle_rad))) )
#define armTorque_nm2BsForce_N(torque, angle_rad) ((float)(torque) / linkage1LeverArm(angle_rad)) //TODO: will be slightly off for actuator 2
#define actuatorForce2ArmTorque(force, angle_rad) ((float)(force) * linkage1LeverArm(angle_rad)) //TODO: will be slightly off for actuator 2
#define armGravityTorque(angle_rad) (COMBINED_ARM_COM_MASS*G*COMBINED_ARM_COM_DIST*sin( (float)(angle_rad)+COMBINED_ARM_COM_ANG_OFFSET+MOUNT_ANG_OFFSET) ) //negative from theta+180, negative from torque direction
#define motorCurrent2Torque(current)  ((float)(current)*TORQUE_CONSTANT)
#define BsForce2motorCurrent(force) motorTorque2current(BsForce2motorTorque((float)(force)))
#define motorCurrent2BsForce(current) motorTorque2BsForce(motorCurrent2Torque((float)(current)))
#define voltage2LoadCellForce_N(voltage) (((((float)(voltage) - LC_VOFFSET)/LC_GAIN)*LC_RATED_FORCE_N*-1)/(LC_SENSITIVITY*LC_VEXCITE))
#define voltage2EPower_W(voltage) ( ((float)(voltage) - EPOWER_VOFFSET)*V_BUS / (EPOWER_GAIN*R_SENSE_OHMS))
#define voltage2Current(voltage) ( ((float)(voltage) - EPOWER_VOFFSET) / (EPOWER_GAIN*R_SENSE_OHMS))
#define gravityCompForce(arm_angle_rad) (armTorque_nm2BsForce_N( armGravityTorque(arm_angle_rad), (arm_angle_rad) ))
#define latency_ms2queueSize(lat) ( (int) (round((float)(lat)*2*CONTROL_FREQ_HZ-1)/2) )

//shared memory info
#define SHMNAM_ASS "ASS"
#define ASS_FIFO "/dev/rtf0"
#define SHMNAM_ASS2 "ASS2"
#define ASS_FIFO2 "/dev/rtf1"

#define ACTUATOR1 0
#define ACTUATOR2 1

#define ACTUATOR1_MASK 0x1
#define ACTUATOR2_MASK 0x2

#define SAFE 1
#define UNSAFE 0

#endif //_ACTUATOR_PARAMS_H







