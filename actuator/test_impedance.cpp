//impedance control
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <analog_in.h>
#include <digital_out.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include <referenceSignals.h>
#include "actuator.hpp"
#include "actuator_utils.h"
#include <csvParser.hpp>

#define RTAI_TASK_NAME "ICNTRL" //must be unique

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define TIME_TO_BEGIN 10 //wait this long before starting test

#define CSV_TIME_IDX 1
#define CSV_POS_IDX 5
#define CSV_VEL_IDX 6

//reference params
static float Switching_freq_hz = P_SETPOINT_SWITCH_FREQ_HZ_DEFAULT;
static float Switching_amp_m = P_SETPOINT_POS_AMPLITUDE_M_DEFAULT;
static float Chirp_upper_freq_hz = P_SWEEP_FREQ_HZ_HIGH;
static float Chirp_rate = P_SWEEP_RATE;
static unsigned char Mode = SINE;
static char ActuatorSel = ACTUATOR1_MASK;
static float Stiffness=0;
static float Damping=0;
static char Safeness=SAFE;
static float Switching_offset_m=MID_ACTUATOR2_POS_M; //close enough for actuator 2 also
static unsigned char SigFromFile=0;
static float Latency_prop = 0.0, Latency_deriv = 0.0;

static unsigned char End=0;
static void endme(int dummy) { End=1; }

//csv file stuff (for signal input)
csv_entry timeStamp, posDes, velDes;

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf (" Reference signal options:\n");
  printf ("  Signal type options (choose one, sine wave is default):\n");
  printf ("  -s                   : squarewave setpoint (default: %d)\n", 0);
  printf ("  -c                   : constant (default: %d)\n", 0);
  printf ("  -p                   : chirp (default: %d)\n", 0);
  printf ("  -e <filename>        : read from external file (default: %d)\n", 0);
  printf ("    for this option, file should contain rows in the form: time,desPos,desVel\n");
  printf ("  Generic signal options:\n");
  printf ("  -a <amp (m)>         : setpoint switching amplitude (default: %f)\n", Switching_amp_m);
  printf ("  -o <offset (m)>      : setpoint switching offset (default: %f)\n", Switching_offset_m);
  printf ("  Periodic signal options:\n");
  printf ("  -f <freq (hz)>       : setpoint switching frequency (default: %f)\n", Switching_freq_hz);
  printf ("  Chirp signal options:\n");
  printf ("  -q <freq (hz)>       : chirp high frequency (default: %f)\n", Chirp_upper_freq_hz);
  printf ("  -r <rate>            : chirp rate (default: %f)\n", Chirp_rate);
  printf ("\n Actuator impedance options:\n");
  printf ("  -k <stiffness (N/m)> : setpoint switching amplitude (default: %f)\n", Stiffness);
  printf ("  -b <damping (Ns/m)>  : setpoint switching amplitude (default: %f)\n", Damping);
  printf ("\n Artificial latency options:\n");
  printf ("  -g <latency (sec)>   : proportional impedance feedback latency (default: %f)\n", Latency_prop);
  printf ("  -d <latency (sec)>   : derivative impedance feedback latency (default: %f)\n", Latency_deriv);
  printf ("\n Actuator selection and safety options:\n");
  printf ("  -t <actuator>        : select actuator, 1=actuator1, 2=actuator2, 3=both (default: %d)\n", ActuatorSel);
  printf ("  -u                   : unsafe, disables position and force limits, don't use with motor power on (default: %d)\n", !(Safeness && Safeness));
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "a:f:t:k:e:b:r:q:o:g:d:scpuh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'f': 
          Switching_freq_hz = atof(optarg);
          break;
      case 'a': 
          Switching_amp_m = atof(optarg);
          break;
      case 'o': 
          Switching_offset_m = atof(optarg);
          break;
      case 'k': 
          Stiffness = atof(optarg);
          break;
      case 'b': 
          Damping = atof(optarg);
          break;
      case 'r': 
          Chirp_rate = atof(optarg);
          break;
      case 'e': 
          SigFromFile = 1;
          printf("Reading signal data from %s\n", optarg);
          readCSVFile(optarg, 3, &timeStamp, &posDes, &velDes);
          break;
      case 'q': 
          Chirp_upper_freq_hz = atof(optarg);
          break;
      case 'g': 
          Latency_prop = atof(optarg);
          break;
      case 'd': 
          Latency_deriv = atof(optarg);
          break;
      case 's': 
          Mode = SQUARE;
          break;
      case 'c':
          Mode = CONST;
          break;
      case 'p':
          Mode = CHIRP;
          break;
      case 'u':
          Safeness = UNSAFE;
          break;
      case 't':
          ActuatorSel = atoi(optarg);
          break;
      case '?': // help
      case 'h': // help
      case ':':
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

int main(int argc, char **argv) {

   int timer_already_running;
   float startCountDown=TIME_TO_BEGIN;
   float desiredPosition_m, desiredVelocity_mps, desiredForce_n;
   int queueSize=0;

   //csv file signal input
   timeStamp.idx = CSV_TIME_IDX;
   posDes.idx = CSV_POS_IDX;
   velDes.idx = CSV_VEL_IDX;

   SEA_Actuator actuator1;
   Actuator_state_str *ass;
   unsigned int rtf_fd;

   Rigid_Actuator actuator2;
   Actuator_state_str *ass2;
   unsigned int rtf_fd2;
   

   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;
 
   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv); //check command line params

   //artificial latency calc
   queueSize = latency_ms2queueSize(Latency_prop);
   if(queueSize < 0) queueSize = 0;
   actuator1.posQueueSize = queueSize;
   actuator2.posQueueSize = queueSize;
   
   queueSize = latency_ms2queueSize(Latency_deriv);
   if(queueSize < 0) queueSize = 0;
   actuator1.velQueueSize = queueSize;
   actuator2.velQueueSize = queueSize;
   printf("Latency set to prop=%.4f sec (%d queue size) deriv=%.4f sec (%d queue size)\n", Latency_prop, actuator1.posQueueSize, Latency_deriv, actuator1.velQueueSize);

   //shared memory setup
   ass = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd,1);

   ass2 = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS2), sizeof(Actuator_state_str));
   rtf_fd2 = open(ASS_FIFO2, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd2,1);

   //mode-specific config

   //safety
   if(Safeness){
     printf("Position and force limits enabled: Fmax=%.1fN, TrqMax=%.1fNm\n", (float)MAX_FORCE_N, (float)MAX_TORQUE_NM);
   }
   else{
     printf("Position and force limits disabled\n");
   }

   //mode
   printf("Controlling impedance at %d hz with stiffness=%.1f damping=%.1f and ", (int)CONTROL_FREQ_HZ, Stiffness, Damping);
   if(Mode == CONST){
     printf("constant position amp=%.3f (%.2flbs)\n", Switching_amp_m, kg2lbs(ADDED_MASS));
   }
   else if(Mode == CHIRP){
     Switching_freq_hz = Chirp_upper_freq_hz;
     printf("chirp amp=%.3f max_freq=%.3f rate=%.3f (%.2flbs)\n", Switching_amp_m, Switching_freq_hz, Chirp_rate, kg2lbs(ADDED_MASS));
   }
   else if(Mode == SQUARE){
     printf("square setpoint amp=%.3f freq=%.3f (%.2flbs)\n", Switching_amp_m, Switching_freq_hz, kg2lbs(ADDED_MASS));
   }
   else if(!SigFromFile){
     printf("sine setpoint amp=%.3f freq=%.3f (%.2flbs)\n", Switching_amp_m, Switching_freq_hz, kg2lbs(ADDED_MASS));
   }
   else {}

   //actuator selection
   if(ActuatorSel & ACTUATOR1_MASK) {
     printf("PD1 enabled: %d\n", FCONTROL1_EN_PD);
     printf("DOB1 enabled: %d\n", FCONTROL1_EN_DOB);
     printf("Kp=%.3f Kd=%.5f Fkd_FC=%.1f Q_FC=%.1f ", (float)FCONTROL1_KP, (float)FCONTROL1_KD, (float)FCONTROL1_KD_FC, (float)FCONTROL1_Q_FC);
   }

   if(ActuatorSel & ACTUATOR2_MASK) {
     printf("PD2 enabled: %d\n", FCONTROL2_EN_PD);
     printf("DOB2 enabled: %d\n", FCONTROL2_EN_DOB);
     printf("Kp=%.3f Kd=%.5f Fkd_FC=%.1f Q_FC=%.1f ", (float)FCONTROL2_KP, (float)FCONTROL2_KD, (float)FCONTROL2_KD_FC, (float)FCONTROL2_Q_FC);
   }
   printf("V_fc=%.1f\n", (float)VEL_ACTUATOR_FC);

   //printf("arm inertia: %f\n", ARM_INERTIA);

   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   AnalogIn_Init();
   Quad_init(512, 20000, 500, 500, 500, 16384, 500, 500);
   //Quad_resetCounters();
   //quadGetOffset(); //issue with resetting spring incremental encoder, do it this way
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   setMotorCurrents(0.0, 0.0);
   AnalogOut_Enable(); //enables analog outputs
#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif
   
   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }

   //timer initialization
   if ( (timer_already_running = rt_is_hard_timer_running()) ){
    printf("timer is already running\n");
    period = nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC);
   }
   else {
    printf("timer is not already running, starting timer\n");
    rt_set_periodic_mode();
    period = start_rt_timer(nano2count(NS_PER_SEC * CONTROL_PERIOD_SEC));
   }

   //wait to begin
   printf("waiting %d seconds to begin..go hold the arm\n", TIME_TO_BEGIN);

   while(startCountDown > 0.0 && !End){
     printf("%0.0f seconds to start\n",startCountDown);
     startCountDown -= 1.0; 
     sleep(1);
   }
   
    //keep program from paging
   mlockall(MCL_CURRENT | MCL_FUTURE); 
   rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   printf("starting...\n");
   fflush(stdout);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;

   while(!End){

     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);
     //printf("sample period %lf\n", samplePeriod_s);
     if(samplePeriod_s > 0.002) printf(":( %f\t%f\n", samplePeriod_s, elapsedTime_s); //check for jitter

     //printf("elapsedTime:%f\n", elapsedTime_s);
     actuator1.setSampleTime(elapsedTime_s, samplePeriod_s);
     actuator2.setSampleTime(elapsedTime_s, samplePeriod_s);

     //read actuator state and put in local struct
     if(ActuatorSel & ACTUATOR1_MASK) {
       if(actuator1.updateState(Safeness)){
         End = 1;
         break;
       }
  
       if(SigFromFile){
         getDesiredPositionFromArray(&desiredPosition_m, &desiredVelocity_mps, elapsedTime_s, actuator1.getActuatorPos(), timeStamp.data, posDes.data, velDes.data, true, &End);
       }
       else{
         getDesiredPosition(&desiredPosition_m, &desiredVelocity_mps, elapsedTime_s, actuator1.getActuatorPos(), Switching_offset_m, Switching_amp_m, Switching_freq_hz, P_SWEEP_RATE, Mode, true, &End);
       }
       desiredForce_n = gravityCompForce(actuator1.getArmAngle()); //gravity compensation force
       //desiredForce_n = 0;

       actuator1.servoImpedance(desiredPosition_m, desiredVelocity_mps, desiredForce_n, Stiffness, Damping);

       setMotor1Current(actuator1.getDesiredMotorCurrent());

       //write actuator state to shared mem
       ass_writeShm(rtf_fd, ass, actuator1.getState());
     }

     if(ActuatorSel & ACTUATOR2_MASK) {
       if(actuator2.updateState(Safeness)){
         End = 1;
         break;
       }

       if(SigFromFile){
         getDesiredPositionFromArray(&desiredPosition_m, &desiredVelocity_mps, elapsedTime_s, actuator2.getActuatorPos(), timeStamp.data, posDes.data, velDes.data, true, &End);
       }
       else{
         getDesiredPosition(&desiredPosition_m, &desiredVelocity_mps, elapsedTime_s, actuator2.getActuatorPos(), Switching_offset_m, Switching_amp_m, Switching_freq_hz, P_SWEEP_RATE, Mode, true, &End);
       }
       desiredForce_n = gravityCompForce(actuator2.getArmAngle()); //gravity compensation force
       //desiredForce_n = 0;

       actuator2.servoImpedance(desiredPosition_m, desiredVelocity_mps, desiredForce_n, Stiffness, Damping);

       setMotor2Current(actuator2.getDesiredMotorCurrent());

       //write actuator state to shared mem
       ass_writeShm(rtf_fd2, ass2, actuator2.getState());
     }
     
     //printf("wrote to shm at t=%f\n",elapsedTime_s);
     //fflush(stdout);

#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     rt_task_wait_period();
   }

   rt_make_soft_real_time();
   if ( !timer_already_running ){
    printf("stopping timer\n");
    stop_rt_timer();
   }
   rt_task_delete(task);
   rtai_free(nam2num(SHMNAM_ASS), ass);
   rtai_free(nam2num(SHMNAM_ASS2), ass2);
   close(rtf_fd);
   close(rtf_fd2);
   printf("ending...\n");
   setMotorCurrents(0.0, 0.0);
   //AnalogOut_Disable();
   

   return 0;
}



