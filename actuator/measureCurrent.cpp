//measure state of the actuator
//prints measurements to both the terminal and to file
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h> 
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <utils.h>
#include "actuator_params.h"

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define SAMPLE_PERIOD_MS 100 //10hz
#define SAMPLE_DURATION_S 0 
#define CONSOLE_DELIMITER "\t"
#define LOGFILE_DEFAULT "current_data.csv" //tab-separated values
#define FILE_DELIMITER ","

static char *LogFile;
static char File_default[] = LOGFILE_DEFAULT;

static int LogRate = SAMPLE_PERIOD_MS;
static int LogDuration = SAMPLE_DURATION_S;

static int End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -r <log rate (ms)>      : sample period (default :%d)\n", LogRate);
  printf ("  -d <log duration (s)>   : sample duration, 0 for infinite (default: %d)\n", LogDuration);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "r:f:d:wsh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'r':
        LogRate = atoi(optarg);
        break;
      case 'd':
        LogDuration = atoi(optarg);
        break;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

void logHeader(FILE* ifp){
  fprintf(ifp, "Time (sec)" FILE_DELIMITER 
    "Sample Period (sec)" FILE_DELIMITER  
    "Current 1 (Amps)" FILE_DELIMITER  
    "Current 2 (Amps)\n");
}

int main(int argc, char **argv) {

   float current1, current2;
   float adc_voltage;

   struct timeval tvStart, tvCurrent;
   long int elapsedTime_us, currentTime_us, sampleTime_prev_us=0;
   double samplePeriod_s, elapsedTime_s;

   int iteration = 0, maxIteration = -1;

   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv);

   //file stuff
   LogFile = File_default;
   FILE* ifp;

   ifp = fopen(LogFile, "w");
   if (!ifp) {
     fprintf(stderr, "Cannot open file %s\n", LogFile);
     exit(1);
   }

   printf("Data logging to "); 
   printf("%s ", LogFile); 
   logHeader(ifp);
   printf("at %d hz\n", (int)(1.0/(LogRate/1000.0)));

   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogIn_Init();

#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif

   //get starting time
   gettimeofday(&tvStart, NULL);
   sampleTime_prev_us = tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec;

   if(LogDuration > 0) {
    maxIteration = LogDuration*1000/LogRate;
   }

   //print header
   printf("Current 1 (Amps)" CONSOLE_DELIMITER CONSOLE_DELIMITER "Current 2 (Amps)\n");

   while(!End && iteration != maxIteration){
     gettimeofday(&tvCurrent, NULL);
     currentTime_us = tvCurrent.tv_sec*USEC_PER_SEC + tvCurrent.tv_usec;
     elapsedTime_us = currentTime_us - (tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec);
     samplePeriod_s = us2sec(currentTime_us - sampleTime_prev_us);
     sampleTime_prev_us = currentTime_us;
     elapsedTime_s = us2sec(elapsedTime_us);
   
     //read first current data
     if( sampleADC_5VU(A_EPOWER_CHAN, &adc_voltage) == ERR_NONE ) {
      //printf("voltage for chan %d: %f\n", A_EPOWER_CHAN, adc_voltage);
      current1 = voltage2Current(adc_voltage);
      //return ERR_NONE;
     }
     else {
       printf("Analog_In: Uh oh..sample error\n");
       //return ERR;
     }

     //read second current data
     if( sampleADC_5VU(A_EPOWER2_CHAN, &adc_voltage) == ERR_NONE ) {
      //printf("voltage for chan %d: %f\n", A_EPOWER2_CHAN, adc_voltage);
      current2 = voltage2Current(adc_voltage);
      //return ERR_NONE;
     }
     else {
       printf("Analog_In: Uh oh..sample error\n");
       //return ERR;
     }

     printf("%f" CONSOLE_DELIMITER CONSOLE_DELIMITER CONSOLE_DELIMITER "%f\n", current1, current2);
     fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", (float)elapsedTime_s, (float)samplePeriod_s, current1, current2);

     iteration++;
#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     usleep(USEC_PER_MSEC*LogRate);
   }

   fclose(ifp);
   printf("ending...\n");

   return 0;
}



