//measures digital filter output
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include <referenceSignals.h>
#include <classyFilters.hpp>
#include <rtai_msg.h>
#include "actuator.hpp"

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define RTAI_TASK_NAME "FILT" //must be unique

#define SAMPLE_FREQ_HZ 1000
#define LOGFILE_DEFAULT "filter_data.csv" //tab-separated values
#define FILE_DELIMITER ","
#define CONSOLE_DELIMITER "\t"

//reference params
#define SWEEP_AMPLITUDE_A 100.0
#define SWEEP_OFFSET_A 0.0
#define SWEEP_FREQ_LOW_HZ 0.1
#define SWEEP_FREQ_HIGH_HZ 500.0
#define SWEEP_RATE 0.003 //percent change of sweep range per second

static float Sweep_ampl_A = SWEEP_AMPLITUDE_A;
static float Sweep_offset_A = SWEEP_OFFSET_A;
static float Sweep_freqLow_hz = SWEEP_FREQ_LOW_HZ;
static float Sweep_freqHigh_hz = SWEEP_FREQ_HIGH_HZ;
static float Sweep_rate = SWEEP_RATE;

static char *LogFile;
static char File_default[] = LOGFILE_DEFAULT;

static int SampleFreq_Hz = SAMPLE_FREQ_HZ;

static unsigned char End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -r <sample rate (hz)>   : sample freq (default :%d)\n", SampleFreq_Hz);
  printf ("  -f <log file>           : file to log to (default: %s)\n", File_default);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "r:f:h?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'r':
        SampleFreq_Hz = atoi(optarg);
        break;
      case 'f':
        LogFile = optarg;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args


int main(int argc, char **argv) {
   float inputSig, kd_out, q_out, DOB_out, qpd_out, pff_out, pdob_out;

   int timer_already_running;
   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;

   //filter definitions
   VelocityFilter kd_Filt(1, 1/(float)SampleFreq_Hz);
   LowPassFilter q_Filt(FCONTROL1_Q_FILT_LEN, 1/(float)SampleFreq_Hz);
   TorqueControlDOB_Filter tdobFilt(FCONTROL1_TDOB_FILT_LEN, 1.0/(float)SampleFreq_Hz);
   LowPassFilter qpd_Filt(PCONTROL1_QP_FILT_LEN, 1/(float)SampleFreq_Hz);
   MassDamperForce_Filter Pff_filt(2, 1/(float)SampleFreq_Hz);
   PositionControlDOB_Filter PDOB_filt(PCONTROL1_PDOB_FILT_LEN, 1/(float)SampleFreq_Hz);

   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv);

   printf("\nFilter params:\n");
   printf("kd_filt: fc=%d Hz\n", FCONTROL1_KD_FC);
   kd_Filt.calculateCoeffs(FCONTROL1_KD_FC);
   kd_Filt.reset();

   printf("q_Filt: fc=%d Hz\n", FCONTROL1_Q_FC);
   q_Filt.calculateCoeffs(FCONTROL1_Q_FC);
   q_Filt.reset();

   printf("tdobFilt: fc=%d Hz, m=%d, b=%d, k=%d, kp=%0.2f Hz, kd=%0.3f Hz, kd_fc=%d Hz, N=%0.1f Hz, eff=%0.1f Hz, k_tau=%0.3f\n", 
      FCONTROL1_Q_FC, M_FIT, B_FIT, SPRING_CONST_NpM, FCONTROL1_KP, FCONTROL1_KD, FCONTROL1_KD_FC, SPEED_REDUCTION, DRIVETRAIN_EFF, TORQUE_CONSTANT);
   tdobFilt.calculateCoeffs(FCONTROL1_Q_FC, M_FIT, B_FIT, SPRING_CONST_NpM, FCONTROL1_KP, FCONTROL1_KD, FCONTROL1_KD_FC, SPEED_REDUCTION, DRIVETRAIN_EFF, TORQUE_CONSTANT);
   tdobFilt.reset();

   printf("qpd_Filt: fc=%d Hz\n", PCONTROL1_DOB_FC);
   qpd_Filt.calculateCoeffs(PCONTROL1_DOB_FC);
   qpd_Filt.reset();

   printf("Pff_filt: fc=%d Hz, m=%0.3f, b=%0.3f \n", PCONTROL1_FC, ARM_INERTIA, ARM_FRICTION);
   Pff_filt.calculateCoeffs(PCONTROL1_FC, ARM_INERTIA, ARM_FRICTION );
   Pff_filt.reset();

   printf("pdobFilt: qp_fc=%d Hz, qpd_fc=%d Hz, m=%d, b=%d, k=%d, kp=%0.2f Hz, kd=%0.3f Hz, kd_fc=%d Hz, N=%0.1f Hz, eff=%0.1f Hz, k_tau=%0.3f\n", 
      PCONTROL1_FC, PCONTROL1_DOB_FC, M_FIT, B_FIT, SPRING_CONST_NpM, FCONTROL1_KP, FCONTROL1_KD, FCONTROL1_KD_FC, SPEED_REDUCTION, DRIVETRAIN_EFF, TORQUE_CONSTANT);
   PDOB_filt.calculateCoeffs(PCONTROL1_FC, PCONTROL1_DOB_FC, M_FIT, B_FIT, SPRING_CONST_NpM, FCONTROL1_KP, FCONTROL1_KD, FCONTROL1_KD_FC, SPEED_REDUCTION, DRIVETRAIN_EFF, TORQUE_CONSTANT);
   PDOB_filt.reset();

   //file stuff
   LogFile = File_default;
   FILE* ifp;
   ifp = fopen(LogFile, "w");
   if (!ifp) {
     fprintf(stderr, "Cannot open file %s\n", LogFile);
     exit(1);
   }

   //print header
   printf("Data logging to %s with freq = %d hz\n", LogFile, SampleFreq_Hz);
   fprintf(ifp, "Time (sec)" FILE_DELIMITER "Sample Period (sec)" FILE_DELIMITER "input signal" FILE_DELIMITER "kd out" 
      FILE_DELIMITER "q out" FILE_DELIMITER "tdob out" FILE_DELIMITER "qpd out" FILE_DELIMITER "pff out" FILE_DELIMITER "pdob out\n");

   

#if TOGGLE_DOUT_ON_CYCLE
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   DigitalOut_Init();
#endif

   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }

   //timer initialization
   if ( (timer_already_running = rt_is_hard_timer_running()) ){
    printf("timer is already running\n");
    period = nano2count(NS_PER_SEC * (1.0/(float)SampleFreq_Hz) );
   }
   else {
    printf("timer is not already running, starting timer\n");
    rt_set_periodic_mode();
    period = start_rt_timer(nano2count(NS_PER_SEC * (1.0/(float)SampleFreq_Hz) ));
   }

   //mlockall(MCL_CURRENT | MCL_FUTURE); //keep program from paging
   //rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   printf("starting...\n");
   fflush(stdout);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;

   while(!End){
     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);

     inputSig = getChirpSignal(elapsedTime_s, samplePeriod_s, Sweep_ampl_A, Sweep_offset_A, Sweep_freqLow_hz, Sweep_freqHigh_hz, Sweep_rate, &End);
     kd_out = FCONTROL1_KD*kd_Filt.next(inputSig);
     q_out = q_Filt.next(inputSig);
     DOB_out = tdobFilt.next(inputSig);
     qpd_out = qpd_Filt.next(inputSig);
     pff_out = Pff_filt.next(inputSig);
     pdob_out = PDOB_filt.next(inputSig);

     fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER  "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER  "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", 
      elapsedTime_s, samplePeriod_s, inputSig, kd_out, q_out, DOB_out, qpd_out, pff_out, pdob_out);
   
#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     rt_task_wait_period();
   }

   rt_make_soft_real_time();
   if ( !timer_already_running ){
    printf("stopping timer\n");
    stop_rt_timer();
   }
   rt_task_delete(task);
   printf("ending...\ndata written to: %s\n",LogFile);
   fclose(ifp);

   return 0;
}



