//measure state of the actuator
//prints measurements to both the terminal and to file
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator.hpp"
#include "actuator_params.h"
#include "actuator_utils.h"
#include <rtai_shm.h>
#include <rtai_fifos.h>

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define SAMPLE_PERIOD_MS 100 //10hz
#define SAMPLE_DURATION_S 0 
#define CONSOLE_DELIMITER "\t"

static int LogRate = SAMPLE_PERIOD_MS;
static int LogDuration = SAMPLE_DURATION_S;

static int End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -r <log rate (ms)>      : sample period (default :%d)\n", LogRate);
  printf ("  -d <log duration (s)>   : sample duration, 0 for infinite (default: %d)\n", LogDuration);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "r:f:d:wsh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'r':
        LogRate = atoi(optarg);
        break;
      case 'd':
        LogDuration = atoi(optarg);
        break;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args


int main(int argc, char **argv) {

   SEA_Actuator actuator1;
   Actuator_state_str *ass;
   unsigned int rtf_fd;

   Rigid_Actuator actuator2;
   Actuator_state_str *ass2;
   unsigned int rtf_fd2;

   struct timeval tvStart, tvCurrent;
   long int elapsedTime_us, elapsedTime_s, currentTime_us, sampleTime_prev_us=0;
   double samplePeriod_s;

   int iteration = 0, maxIteration = -1;

   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv);

   ass = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd,1);

   ass2 = (Actuator_state_str*)rtai_malloc(nam2num(SHMNAM_ASS2), sizeof(Actuator_state_str));
   rtf_fd2 = open(ASS_FIFO2, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd2,1);

   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   AnalogIn_Init();
   Quad_init(512, 20000, 500, 500, 500, 16384, 500, 500);
   //Quad_resetCounters();
   //quadGetOffset(); //issue with resetting spring incremental encoder, do it this way
#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif

   //get starting time
   gettimeofday(&tvStart, NULL);
   sampleTime_prev_us = tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec;

   if(LogDuration > 0) {
    maxIteration = LogDuration*1000/LogRate;
   }

   //print header
   printf("Arm Angle(deg)" CONSOLE_DELIMITER CONSOLE_DELIMITER CONSOLE_DELIMITER "Actuator Force(N)" CONSOLE_DELIMITER CONSOLE_DELIMITER CONSOLE_DELIMITER "LoadCell Force(N)\n");
   printf("Actuator 1" CONSOLE_DELIMITER "Actuator 2" CONSOLE_DELIMITER "Actuator 1" CONSOLE_DELIMITER "Actuator 2" CONSOLE_DELIMITER "Actuator 1" CONSOLE_DELIMITER "Actuator 2\n");

   while(!End && iteration != maxIteration){
     gettimeofday(&tvCurrent, NULL);
     currentTime_us = tvCurrent.tv_sec*USEC_PER_SEC + tvCurrent.tv_usec;
     elapsedTime_us = currentTime_us - (tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec);
     samplePeriod_s = us2sec(currentTime_us - sampleTime_prev_us);
     sampleTime_prev_us = currentTime_us;
     elapsedTime_s = us2sec(elapsedTime_us);

     actuator1.setSampleTime(elapsedTime_s, samplePeriod_s);
     actuator2.setSampleTime(elapsedTime_s, samplePeriod_s);
   
     //read actuator state and put in local struct
     if(actuator1.updateState(UNSAFE)){
       End = 1;
       break;
     }

     if(actuator2.updateState(UNSAFE)){
       End = 1;
       break;
     }

     printf("%f" CONSOLE_DELIMITER "%f" CONSOLE_DELIMITER "%f" CONSOLE_DELIMITER "%f" CONSOLE_DELIMITER "%f" CONSOLE_DELIMITER "%f\n", 
      rad2deg(actuator1.getArmAngle()), 
      rad2deg(actuator2.getArmAngle()), 
      actuator1.getForce(), 
      actuator2.getForce(), 
      actuator1.getLoadcellForce(), 
      actuator2.getLoadcellForce());
   
     //write actuator state to shared mem
     ass_writeShm(rtf_fd, ass, actuator1.getState());
     ass_writeShm(rtf_fd, ass, actuator2.getState());

     iteration++;
#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     usleep(USEC_PER_MSEC*LogRate);
   }

   printf("ending...\n");
   rtai_free(nam2num(SHMNAM_ASS), Ass);
   rtai_free(nam2num(SHMNAM_ASS2), Ass2);

   return 0;
}



